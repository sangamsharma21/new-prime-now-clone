import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:prime_now_clone/Utils/colors.dart';

class ChangeLocation extends StatefulWidget {
  @override
  _ChangeLocationState createState() => _ChangeLocationState();
}

class _ChangeLocationState extends State<ChangeLocation> {
  var phoneNumber = '';
  TextEditingController _editingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        elevation: 0.0,
        brightness: Brightness.light,
        centerTitle: true,
        title: Text(
          'Change Location',
          style: TextStyle(
              fontSize: 16.0,
              color: Colors.black,
              fontFamily: 'Roboto',
              fontWeight: FontWeight.bold),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          height: size.height,
          width: size.width,
          child: Stack(
            children: [
              Column(
                children: [
                  Container(
                    padding: const EdgeInsets.all(12.0),
                    margin: EdgeInsets.only(top: 20, left: 30, right: 30),
                    child: Image.asset(
                      'assets/images/logo.png',
                      width: size.width,
                      height: size.height / 6,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Text(
                      'Where do you want to shop?',
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          fontFamily: 'Roboto',
                          color: Colors.black54),
                    ),
                  ),
                  GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () {
                      //TODO: please make a function for Calling Bottomsheet
                      showModalBottomSheet(
                          context: context,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(0.0),
                                  topRight: Radius.circular(0.0))),
                          builder: (BuildContext context) {
                            return Container(
                              padding: EdgeInsets.symmetric(vertical: 20),
                              child: Wrap(
                                children: [
                                  Center(
                                    child: Container(
                                      child: Image.asset(
                                        'assets/images/placeholder.png',
                                        color: Colors.orange,
                                        width: 50,
                                        height: 50,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.all(10.0),
                                    child: Center(
                                      child: Text(
                                        'Turn on location access, '
                                        'and we\'ll let you know anytime you changes '
                                        'locations so you can see products available in your area',
                                        style: TextStyle(
                                            color: Colors.black54,
                                            fontSize: 14.0,
                                            fontFamily: 'Roboto'),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    width: size.width,
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 30, vertical: 10),
                                    height: 40,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        width: 0.5,
                                      ),
                                      borderRadius: BorderRadius.circular(5.0),
                                      gradient: LinearGradient(
                                          begin: Alignment.topCenter,
                                          end: Alignment.bottomCenter,
                                          colors: [
                                            CustomColors.lightYellow,
                                            CustomColors.yellow
                                          ]),
                                    ),
                                    child: Center(
                                      child: Text(
                                        "Turn on Location Access",
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontFamily: 'Roboto',
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ),
                                  GestureDetector(
                                    child: Container(
                                        margin: EdgeInsets.only(top: 10.0),
                                        child: Center(
                                          child: Text(
                                            'Not Now',
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.blue),
                                          ),
                                        )),
                                    behavior: HitTestBehavior.opaque,
                                    onTap: () {
                                      Navigator.pop(context);
                                    },
                                  )
                                ],
                              ),
                            );
                          });
                    },
                    child: Container(
                      width: size.width,
                      margin:
                          EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                      height: 40,
                      decoration: BoxDecoration(
                        border: Border.all(
                          width: 0.5,
                        ),
                        borderRadius: BorderRadius.circular(5.0),
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              CustomColors.lightYellow,
                              CustomColors.yellow
                            ]),
                      ),
                      child: Center(
                        child: Text(
                          "Use my Location",
                          style: TextStyle(
                              fontSize: 16.0,
                              fontFamily: 'Roboto',
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          color: Colors.grey.shade200,
                          height: 1.0,
                          width: size.width / 3,
                          margin: EdgeInsets.only(right: 10, left: 30),
                        ),
                        Text('Or'),
                        Container(
                          color: Colors.grey.shade200,
                          height: 1.0,
                          width: size.width / 3,
                          margin: EdgeInsets.only(left: 10, right: 30),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(30, 20, 30, 0),
                    height: 45,
                    child: Row(
                      children: [
                        Expanded(
                          child: TextFormField(
                            controller: _editingController,
                            onChanged: (value) {
                              setState(() {
                                phoneNumber = value;
                              });
                            },
                            keyboardType: TextInputType.phone,
                            decoration: InputDecoration(
                              hintText: 'Enter a ZIP Code',
                              filled: true,
                              fillColor: Colors.grey.shade100,
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: size.width / 4),
                              hintStyle: TextStyle(
                                  color: Colors.black54, fontSize: 14.0),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5.0),
                                  borderSide: BorderSide.none),
                            ),
                          ),
                        ),
                        phoneNumber.isNotEmpty
                            ? Container(
                                margin: EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                    color: Colors.orange,
                                    shape: BoxShape.circle),
                                height: 45,
                                width: 45,
                                child: Icon(
                                  Icons.arrow_forward,
                                  color: Colors.white,
                                  size: 30,
                                ),
                              )
                            : Container()
                      ],
                    ),
                  ),
                ],
              ),
              Positioned(
                  bottom: 20,
                  right: 0,
                  left: 0,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        width: size.width,
                        height: 1.0,
                        margin: EdgeInsets.only(
                          left: 30,
                          right: 30,
                        ),
                        color: Colors.grey.shade100,
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 5.0, bottom: 5.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Image.network(
                              'https://i.pinimg.com/236x/50/f3/9f/50f39febff794741fed9d23e3a489e07.jpg',
                              width: 25,
                              height: 25,
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 5),
                              child: Text(
                                'United States',
                                style: TextStyle(
                                    color: Colors.blue,
                                    fontFamily: 'Roboto',
                                    fontWeight: FontWeight.w500,
                                    fontSize: 16.0),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
