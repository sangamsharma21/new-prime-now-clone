import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:prime_now_clone/Utils/Urls.dart';
import 'package:prime_now_clone/Utils/colors.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';
import 'package:prime_now_clone/Utils/dotsIndicator.dart';

class ProductDetail extends StatefulWidget {
  @override
  _ProductDetailState createState() => _ProductDetailState();
}

class _ProductDetailState extends State<ProductDetail> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final _controller = new PageController();

  static const _kDuration = const Duration(milliseconds: 300);

  static const _kCurve = Curves.ease;

  final _kArrowColor = Colors.black.withOpacity(0.8);

  var productData;
  List productImages;
  var loading = false;

  Future<void> getProductDetail() async {
    var response = await http.get(
      Urls.getProductDetail + '237',
    );

    try {
      setState(() {
        var convertJsonData = json.decode(response.body);
        if (convertJsonData['status'] == true) {
          productData = convertJsonData['data']['product_data'];
          print('Product Data is : $productData');
          productImages = convertJsonData['data']['images'];
          print(
              'Product Images are $productImages and length is ${productImages.length}');
        } else {}
      });
    } catch (e) {
      setState(() {});
    }
  }

  Future<void> addProduct() async {
    var response = await http.post(
      Urls.addProductToCart,
      body: {
        'product_id': '239',
        'user_id': '9',
        'qty': '1',
      },
    );

    try {
      setState(() {
        var convertJsonData = json.decode(response.body);
        if (convertJsonData['status'] == true) {
          loading = false;
          Fluttertoast.showToast(
            msg: 'Added  Successfully',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
          );
        } else {
          loading = false;
          Fluttertoast.showToast(
            msg: 'Something went wrong',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
          );
        }
      });
    } catch (e) {
      setState(() {
        loading = false;
        Fluttertoast.showToast(
          msg: e.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
        );
      });
    }
  }

  final List<Widget> _pages = <Widget>[
    ConstrainedBox(
      constraints: BoxConstraints.expand(),
      child: Container(
        height: 150.0,
        child: Image(
          image: AssetImage("assets/images/logo.png"),
        ),
      ),
    ),
    ConstrainedBox(
      constraints: BoxConstraints.expand(),
      child: Container(
        height: 150.0,
        child: Image(
          image: AssetImage("assets/images/logo.png"),
        ),
      ),
    ),
    ConstrainedBox(
      constraints: BoxConstraints.expand(),
      child: Container(
        height: 150.0,
        child: Image(
          image: AssetImage("assets/images/logo.png"),
        ),
      ),
    ),
    ConstrainedBox(
      constraints: BoxConstraints.expand(),
      child: Container(
        height: 150.0,
        child: Image(
          image: AssetImage("assets/images/logo.png"),
        ),
      ),
    ),
  ];

  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getProductDetail();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: CustomDrawer(),
        key: scaffoldKey,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(kToolbarHeight),
            child: CustomAppbar(
              scaffoldKey: this.scaffoldKey,
            )),
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 0.0),
                  child: Text(
                    productData['sku'],
                    style: Theme.of(context).textTheme.bodyText2,
                  ),
                ),
                SizedBox(
                  height: 2.0,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
                  child: Text(
                    productData['name'],
                    style: Theme.of(context).textTheme.headline1,
                  ),
                ),
                SizedBox(
                  height: 8.0,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
                  height: 200.0,
                  color: CustomColors.grey,
                  child: Stack(
                    children: [
                      Container(
                        child: PageView.builder(
                          itemCount: productImages.length,
                          controller: _controller,
                          itemBuilder: (BuildContext context, int index) {
                            return ConstrainedBox(
                              constraints: BoxConstraints.expand(),
                              child: Container(
                                height: 150.0,
                                child: Image(
                                  image: NetworkImage(
                                      productImages[index]['photo']),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
                  child: DotsIndicator(
                    controller: _controller,
                    itemCount: _pages.length,
                    onPageSelected: (int page) {
                      _controller.animateToPage(
                        page,
                        duration: _kDuration,
                        curve: _kCurve,
                      );
                    },
                  ),
                ),
                SizedBox(
                  height: 8.0,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
                  child: Row(
                    children: [
                      Text(
                        "Price: ",
                        style: Theme.of(context).textTheme.bodyText2,
                      ),
                      Text(
                        "\$ ${productData['price'].toString()}",
                        style:
                            TextStyle(color: CustomColors.red, fontSize: 18.0),
                      ),
                      Text(
                        "(\$0.29 / Ounce)",
                        style:
                            TextStyle(color: CustomColors.red, fontSize: 14.0),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    setState(() {
                      loading = true;
                    });
                    addProduct();
                  },
                  child: Container(
                    margin: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      border: Border.all(width: 0.5),
                      borderRadius: BorderRadius.circular(5.0),
                      gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                            CustomColors.lightYellow,
                            CustomColors.yellow
                          ]),
                    ),
                    child: Padding(
                      padding:
                          const EdgeInsets.fromLTRB(25.0, 15.0, 25.0, 15.0),
                      child: Center(
                        child: loading == false
                            ? Text(
                                "Add to Cart",
                                style: TextStyle(
                                  fontSize: 16.0,
                                ),
                              )
                            : CircularProgressIndicator(),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
                  child: Text(
                    "About this item",
                    style: Theme.of(context).textTheme.headline1,
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    border:
                        Border.all(width: 1, color: CustomColors.darkOffWhite),
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 5.0),
                        child: Text(
                          "Description",
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 10.0),
                        child: Text(
                          productData['slug'],
                          style: Theme.of(context).textTheme.bodyText2,
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 1.0,
                        color: CustomColors.darkOffWhite,
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 5.0),
                        child: Text(
                          "Features & Details",
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 10.0),
                        child: Text(
                          productData['details'],
                          style: Theme.of(context).textTheme.bodyText2,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 5.0),
                        child: Text(
                          "Product Information",
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 10.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width,
                              height: 1.0,
                              color: CustomColors.darkOffWhite,
                            ),
                            Container(
                              child: Row(
                                children: [
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.42,
                                    color: CustomColors.offWhite,
                                    child: Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Text(
                                        "Stock",
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText2,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width * 0.4,
                                    child: Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Text(
                                        productData['stock'].toString(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText2,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              height: 1.0,
                              color: CustomColors.darkOffWhite,
                            ),
                            Container(
                              child: Row(
                                children: [
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.42,
                                    color: CustomColors.offWhite,
                                    child: Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Text(
                                        "Manufactured Date",
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText2,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width * 0.4,
                                    child: Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Text(
                                        productData['created_at'].toString(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText2,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 5.0),
                        child: Text(
                          "Additional Information",
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      ),
                      Padding(
                          padding: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 10.0),
                          child: Row(
                            children: [
                              Text(
                                "Customer Reviews: ",
                                style: Theme.of(context).textTheme.headline6,
                              ),
                              Text(
                                "4.5 out of 5 stars",
                                style: Theme.of(context).textTheme.bodyText2,
                              )
                            ],
                          )),
                      SizedBox(
                        height: 8.0,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 1.0,
                        color: CustomColors.darkOffWhite,
                      ),
                      SizedBox(
                        height: 8.0,
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 5.0),
                        child: Text(
                          "Important Information",
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 5.0),
                        child: Text(
                          "Ingredients",
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 10.0),
                        child: Text(
                          "Organic hearts of romaine",
                          style: Theme.of(context).textTheme.bodyText2,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 5.0),
                        child: Text(
                          "Legal Disclaimer",
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 10.0),
                        child: Text(
                          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. "
                          "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, "
                          "when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
                          style: Theme.of(context).textTheme.bodyText2,
                        ),
                      ),
                      SizedBox(
                        height: 5.0,
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 25.0,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 1.0,
                  color: CustomColors.darkOffWhite,
                ),
                SizedBox(
                  height: 25.0,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            "Customer Reviews",
                            style: Theme.of(context).textTheme.headline1,
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            "4.5 out of 5",
                            style: Theme.of(context).textTheme.bodyText2,
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            "7,823 customer ratings",
                            style: TextStyle(color: CustomColors.grey),
                          ),
                        ],
                      ),
                      Icon(
                        Icons.arrow_forward_ios_rounded,
                        color: CustomColors.grey,
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 25.0,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 1.0,
                  color: CustomColors.darkOffWhite,
                ),
                SizedBox(
                  height: 25.0,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 5.0,
                  color: CustomColors.darkOffWhite,
                ),
                SizedBox(
                  height: 25.0,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
                  child: Text(
                    "Customers who bought this item also bought",
                    style: Theme.of(context).textTheme.headline1,
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 10.0),
                  child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: 3,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        height: 100.0,
                        margin: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 5.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                              width: 100.0,
                              height: 100.0,
                              color: CustomColors.grey,
                              child: Image(
                                image: AssetImage("assets/images/logo.png"),
                              ),
                            ),
                            Flexible(
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: Container(
                                  height: 100.0,
                                  margin: EdgeInsets.only(left: 10.0),
                                  child: Text(
                                    "Greenhouse Wild Wonders Tomatoes, 12 Ounce",
                                    style:
                                        Theme.of(context).textTheme.bodyText2,
                                    textAlign: TextAlign.left,
                                    overflow: TextOverflow.visible,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 5.0,
                  color: CustomColors.darkOffWhite,
                ),
                SizedBox(
                  height: 25.0,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
                  child: Text(
                    "Customers who viewed this item also viewed",
                    style: Theme.of(context).textTheme.headline1,
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 10.0),
                  child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: 3,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        height: 100.0,
                        margin: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 5.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                              width: 100.0,
                              height: 100.0,
                              color: CustomColors.grey,
                              child: Image(
                                image: AssetImage("assets/images/logo.png"),
                              ),
                            ),
                            Flexible(
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: Container(
                                  height: 100.0,
                                  margin: EdgeInsets.only(left: 10.0),
                                  child: Text(
                                    "Greenhouse Wild Wonders Tomatoes, 12 Ounce",
                                    style:
                                        Theme.of(context).textTheme.bodyText2,
                                    textAlign: TextAlign.left,
                                    overflow: TextOverflow.visible,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 5.0),
                  width: MediaQuery.of(context).size.width,
                  height: 1.0,
                  color: CustomColors.darkOffWhite,
                ),
                SizedBox(
                  height: 15.0,
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
                  child: Text(
                    "Disclaimer: ",
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 10.0),
                  child: Text(
                    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. "
                    "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, "
                    "when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
                    style: Theme.of(context).textTheme.bodyText2,
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
