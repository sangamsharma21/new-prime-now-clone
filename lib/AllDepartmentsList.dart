import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:prime_now_clone/ProductList.dart';
import 'package:prime_now_clone/Utils/colors.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';

class AllDepartmentList extends StatefulWidget {
  @override
  _AllDepartmentListState createState() => _AllDepartmentListState();
}

class _AllDepartmentListState extends State<AllDepartmentList> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: CustomDrawer(),
        key: scaffoldKey,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(kToolbarHeight),
            child: CustomAppbar(
              scaffoldKey: this.scaffoldKey,
            )),
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  margin: EdgeInsets.all(15.0),
                  child: Text(
                    "All Departments",
                    style: Theme.of(context).textTheme.headline1,
                  ),
                ),
                Container(
                  child: ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: 15,
                    itemBuilder: (BuildContext context, int index) {
                      return GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ProductList()));
                        },
                        child: Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Container(
                                margin:
                                    EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                                child: Text(
                                  "Baby & Kids",
                                  style: TextStyle(
                                      color: CustomColors.lightGrey,
                                      fontSize: 16.0),
                                ),
                              ),
                              Container(
                                height: 1.0,
                                width: MediaQuery.of(context).size.width,
                                color: CustomColors.darkOffWhite,
                              )
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
