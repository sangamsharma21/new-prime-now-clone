import 'dart:async';

import 'package:flutter/material.dart';
import 'package:prime_now_clone/DashboardScreen.dart';
import 'package:prime_now_clone/views/auth/SignIn.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  setUserIdPref() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      var key = sharedPreferences.getInt("user_id");
    });
    if (sharedPreferences.containsKey('user_id')) {
      Timer(
          Duration(seconds: 2),
          () => Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) => DashboardScreen())));
    } else {
      Timer(
          Duration(seconds: 2),
          () => Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => SignIn())));
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setUserIdPref();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Padding(
          padding: const EdgeInsets.only(left: 30.0, right: 30.0),
          child: Image(image: AssetImage('assets/images/logo.png')),
        ),
      ),
    );
  }
}
