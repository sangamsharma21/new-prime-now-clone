import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:prime_now_clone/DashboardScreen.dart';
import 'package:prime_now_clone/ForgotPassoword.dart';
import 'package:prime_now_clone/Utils/Urls.dart';
import 'package:prime_now_clone/Utils/colors.dart';
import 'package:prime_now_clone/views/auth/SignUp.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _password = TextEditingController();
  TextEditingController _email = TextEditingController();
  var loading = false;
  bool showPassword = false;

  void _showPassword(bool newValue) => setState(() {
        showPassword = newValue;

        if (showPassword) {
        } else {}
      });

  Future<void> signIn(body) async {
    var response = await http.post(
      Urls.Login,
      body: body,
    );

    try {
      setState(() {
        var convertJsonData = json.decode(response.body);
        print('$convertJsonData');
        if (convertJsonData['status'] == true) {
          loading = false;
          Fluttertoast.showToast(
            msg: 'Login In  Successfully',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
          );
          setUserIdPref(convertJsonData['user_data']['id']);
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) => DashboardScreen()));
        } else {
          loading = false;
          Fluttertoast.showToast(
            msg: 'Something went wrong',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
          );
        }
      });
    } catch (e) {
      setState(() {
        loading = false;
        Fluttertoast.showToast(
          msg: e.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
        );
      });
    }
  }

  setUserIdPref(var user_id) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      sharedPreferences.setInt("user_id", user_id);
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Color(0xffecedf1),
        elevation: 1.0,
        title: Image.asset(
          'images/oceansaks.png',
          width: 80,
          height: 40,
        ),
      ),
      body: SingleChildScrollView(
        child: loading == false
            ? Container(
                color: Color(0xfff6f6f6),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 10.0, top: 10),
                            child: Text(
                              'Sign-In',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                              ),
                            ),
                          ),
                        ],
                      ),
                      GestureDetector(
                        behavior: HitTestBehavior.opaque,
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ForgotPassword()));
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: 10),
                              child: Text(
                                'Forgot password?',
                                style: TextStyle(
                                  color: Colors.blue,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 16,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(0.0),
                        width: size.width,
                        margin: EdgeInsets.only(
                          right: 10,
                          left: 10,
                          top: 10,
                        ),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(color: Colors.grey.shade300)),
                        child: TextFormField(
                          controller: _email,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Email cannot be empty';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              hintText: 'Email(phone for mobile accounts)',
                              contentPadding: EdgeInsets.fromLTRB(15, 0, 10, 0),
                              hintStyle: TextStyle(
                                  color: Colors.grey.shade400,
                                  fontFamily: 'Roboto',
                                  fontSize: 16.0)),
                        ),
                      ),
                      Container(
                        width: size.width,
                        padding: EdgeInsets.all(0.0),
                        margin: EdgeInsets.only(
                          right: 10,
                          left: 10,
                        ),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(color: Colors.grey.shade300)),
                        child: TextFormField(
                          controller: _password,
                          validator: (value) {
                            if (value.isEmpty) {
                              return ' Password cannot be empty';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              hintText: 'password',
                              contentPadding: EdgeInsets.fromLTRB(15, 0, 10, 0),
                              hintStyle: TextStyle(
                                  color: Colors.grey.shade400,
                                  fontFamily: 'Roboto',
                                  fontSize: 16.0)),
                        ),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Checkbox(
                              value: showPassword, onChanged: _showPassword),
                          Container(
                            margin: EdgeInsets.only(left: 1.0),
                            child: Text(
                              'Show password',
                              style: TextStyle(
                                  fontSize: 14.0,
                                  fontFamily: 'Roboto',
                                  color: Colors.black),
                            ),
                          )
                        ],
                      ),
                      Container(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Checkbox(
                                value: showPassword, onChanged: _showPassword),
                            Container(
                              margin: EdgeInsets.only(left: 1.0),
                              child: Text(
                                'keep me signed in.',
                                style: TextStyle(
                                    fontSize: 14.0,
                                    fontFamily: 'Roboto',
                                    color: Colors.black),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 1.0, top: 0.0),
                              child: Text(
                                'Details',
                                style: TextStyle(
                                    fontSize: 14.0,
                                    fontFamily: 'Roboto',
                                    color: Colors.blue),
                              ),
                            ),
                          ],
                        ),
                      ),
                      GestureDetector(
                        behavior: HitTestBehavior.opaque,
                        onTap: () {
                          if (_formKey.currentState.validate()) {
                            setState(() {
                              loading = true;
                            });
                            var body = {
                              'email': '${_email.text}',
                              'password': '${_password.text}',
                            };
                            signIn(body);
                          }
                        },
                        child: Container(
                          width: size.width,
                          margin: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          height: 43,
                          decoration: BoxDecoration(
                            border: Border.all(
                              width: 0.5,
                            ),
                            borderRadius: BorderRadius.circular(5.0),
                            gradient: LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: [
                                  CustomColors.lightYellow,
                                  CustomColors.yellow
                                ]),
                          ),
                          child: Center(
                            child: Text(
                              "Sign in",
                              style: TextStyle(
                                  fontSize: 15.0,
                                  fontFamily: 'Roboto',
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.all(10.0),
                        child: Text(
                          'By signing in you are agreeing  to our Conditions of Use and  Sale and our Privacy  Notice. ',
                          style: TextStyle(
                            color: Colors.black,
                            fontFamily: 'Roboto',
                            fontSize: 12.0,
                          ),
                        ),
                      ),
                      Flex(
                        direction: Axis.horizontal,
                        children: [
                          Flexible(
                            flex: 1,
                            child: Container(
                                margin: EdgeInsets.only(left: 10.0, right: 2.0),
                                child: Divider()),
                          ),
                          Text(
                            "New to App",
                            style: TextStyle(
                                fontFamily: 'Roboto',
                                fontSize: 12.0,
                                color: Colors.black54),
                          ),
                          Flexible(
                            flex: 1,
                            child: Container(
                                margin: EdgeInsets.only(right: 10.0, left: 2.0),
                                child: Divider()),
                          ),
                        ],
                      ),
                      GestureDetector(
                        child: Container(
                          width: size.width,
                          margin: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          height: 43,
                          decoration: BoxDecoration(
                            border: Border.all(
                              width: 0.5,
                            ),
                            borderRadius: BorderRadius.circular(5.0),
                            gradient: LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: [Color(0xfff3f4f6), Color(0xffeaebef)]),
                          ),
                          child: Center(
                            child: Text(
                              "Create New Account",
                              style: TextStyle(
                                  fontSize: 14.0,
                                  fontFamily: 'Roboto',
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                        ),
                        behavior: HitTestBehavior.opaque,
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SignUp()));
                        },
                      ),
                      Container(
                        child: Divider(),
                        margin:
                            EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                      ),
                      Text(
                        '1996-2020 ,  inc. or its affiliates ',
                        style: TextStyle(
                          color: Colors.grey.shade500,
                          fontSize: 12.0,
                          fontFamily: 'Roboto',
                        ),
                      )
                    ],
                  ),
                ),
              )
            : Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ),
      ),
    );
  }
}
