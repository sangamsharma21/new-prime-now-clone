import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:prime_now_clone/Utils/Urls.dart';
import 'package:prime_now_clone/Utils/colors.dart';
import 'package:prime_now_clone/views/auth/SignIn.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController _email = TextEditingController();
  TextEditingController _password = TextEditingController();
  TextEditingController _confirmpassword = TextEditingController();
  TextEditingController _name = TextEditingController();
  bool showPassword = false;

  var signUpResponse;

  var loading = false;

  void _showPassword(bool newValue) => setState(() {
        showPassword = newValue;

        if (showPassword) {
        } else {}
      });

  Future<void> postSignUp(body) async {
    var response = await http.post(
      Urls.Register,
      body: body,
    );

    try {
      setState(() {
        var convertJsonData = json.decode(response.body);
        if (convertJsonData['status'] == true) {
          var otp = convertJsonData['otp'];
          print('OTP is $otp');
          verifyOtp(otp);
        } else {
          loading = false;
          Fluttertoast.showToast(
            msg: 'Something went wrong',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
          );
        }
      });
    } catch (e) {
      setState(() {
        loading = false;
        Fluttertoast.showToast(
          msg: e.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
        );
      });
    }
  }

  Future<void> verifyOtp(otp) async {
    var response = await http.post(
      Urls.Verify,
      body: {
        'email': '${_email.text}',
        'password': '${_password.text}',
        'otp': '$otp',
        'name': '${_name.text}',
        'password_confirmation': '${_confirmpassword.text}'
      },
    );

    try {
      setState(() {
        var convertJsonData = json.decode(response.body);
        if (convertJsonData['status'] == true) {
          loading = false;
          Fluttertoast.showToast(
            msg: 'Registered Successfully',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
          );
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => SignIn()));
        } else {
          loading = false;
          Fluttertoast.showToast(
            msg: 'Something went wrong',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
          );
        }
      });
    } catch (e) {
      setState(() {
        loading = false;
        Fluttertoast.showToast(
          msg: e.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
        );
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Color(0xfff6f6f6),
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Color(0xffecedf1),
        elevation: 1.0,
        title: Image.asset(
          'images/oceansaks.png',
          width: 100,
          height: 50,
        ),
      ),
      body: SingleChildScrollView(
        child: loading == false
            ? Container(
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 10.0, top: 10),
                          child: Text(
                            'Create Account',
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Form(
                      key: _formKey,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            padding: EdgeInsets.all(0.0),
                            width: size.width,
                            margin: EdgeInsets.only(
                              right: 10,
                              left: 10,
                              top: 10,
                            ),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                border:
                                    Border.all(color: Colors.grey.shade300)),
                            child: TextFormField(
                              controller: _name,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Name cannot be empty';
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                  enabledBorder: InputBorder.none,
                                  hintText: 'Name',
                                  contentPadding:
                                      EdgeInsets.fromLTRB(15, 0, 10, 0),
                                  hintStyle: TextStyle(
                                      color: Colors.grey.shade400,
                                      fontFamily: 'Roboto',
                                      fontSize: 16.0)),
                            ),
                          ),
                          Container(
                            width: size.width,
                            padding: EdgeInsets.all(0.0),
                            margin:
                                EdgeInsets.only(right: 10, left: 10, top: 10),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                border:
                                    Border.all(color: Colors.grey.shade300)),
                            child: TextFormField(
                              controller: _email,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Email cannot be empty';
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                  enabledBorder: InputBorder.none,
                                  hintText: 'Your Email Address',
                                  contentPadding:
                                      EdgeInsets.fromLTRB(15, 0, 10, 0),
                                  hintStyle: TextStyle(
                                      color: Colors.grey.shade400,
                                      fontFamily: 'Roboto',
                                      fontSize: 16.0)),
                            ),
                          ),
                          Container(
                            width: size.width,
                            padding: EdgeInsets.all(0.0),
                            margin:
                                EdgeInsets.only(right: 10, left: 10, top: 10),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                border:
                                    Border.all(color: Colors.grey.shade300)),
                            child: TextFormField(
                              controller: _password,
                              validator: (value) {
                                if (value.isEmpty && value.length < 6) {
                                  return 'Password be atleast of 6 characters';
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                  enabledBorder: InputBorder.none,
                                  hintText: 'Create Password',
                                  contentPadding:
                                      EdgeInsets.fromLTRB(15, 0, 10, 0),
                                  hintStyle: TextStyle(
                                      color: Colors.grey.shade400,
                                      fontFamily: 'Roboto',
                                      fontSize: 16.0)),
                            ),
                          ),
                          Container(
                            width: size.width,
                            padding: EdgeInsets.all(0.0),
                            margin:
                                EdgeInsets.only(right: 10, left: 10, top: 10),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                border:
                                    Border.all(color: Colors.grey.shade300)),
                            child: TextFormField(
                              controller: _confirmpassword,
                              validator: (value) {
                                if (value.isEmpty && value.length < 6) {
                                  return 'Confirm Password be atleast of 6 characters';
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                  enabledBorder: InputBorder.none,
                                  hintText: 'Confirm Password',
                                  contentPadding:
                                      EdgeInsets.fromLTRB(15, 0, 10, 0),
                                  hintStyle: TextStyle(
                                      color: Colors.grey.shade400,
                                      fontFamily: 'Roboto',
                                      fontSize: 16.0)),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      child: Row(
                        children: [
                          Container(
                            child: Icon(
                              Icons.info_outlined,
                              color: Colors.blueAccent,
                              size: 18,
                            ),
                            margin: EdgeInsets.only(left: 10),
                          ),
                          Container(
                            margin: EdgeInsets.only(right: 10, left: 3.0),
                            child: Text(
                              'Passwords must be at least 6 characters.',
                              style: TextStyle(
                                  fontSize: 12.0,
                                  fontFamily: 'Roboto',
                                  color: Colors.black,
                                  fontWeight: FontWeight.w400),
                            ),
                          )
                        ],
                      ),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Checkbox(value: showPassword, onChanged: _showPassword),
                        Container(
                          margin: EdgeInsets.only(left: 1.0),
                          child: Text(
                            'Show password',
                            style: TextStyle(
                                fontSize: 14.0,
                                fontFamily: 'Roboto',
                                color: Colors.black),
                          ),
                        )
                      ],
                    ),
                    GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: () {
                        if (_formKey.currentState.validate()) {
                          if (_password.text == _confirmpassword.text) {
                            setState(() {
                              loading = true;
                            });
                            var body = {
                              'email': '${_email.text}',
                              'password': '${_password.text}',
                              'password_confirmation':
                                  '${_confirmpassword.text}',
                              'name': '${_name.text}'
                            };
                            postSignUp(body);
                          } else {
                            Fluttertoast.showToast(
                                msg: 'Password Not Matched ',
                                gravity: ToastGravity.BOTTOM);
                          }
                        }
                      },
                      child: Container(
                        width: size.width,
                        margin:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                        height: 43,
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: 0.5,
                          ),
                          borderRadius: BorderRadius.circular(5.0),
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                CustomColors.lightYellow,
                                CustomColors.yellow
                              ]),
                        ),
                        child: Center(
                          child: Text(
                            "Create your  Account",
                            style: TextStyle(
                                fontSize: 15.0,
                                fontFamily: 'Roboto',
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(10.0),
                      child: Text(
                        'By creating an account , you agree to  Conditions of Use and Privacy Notice',
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'Roboto',
                          fontSize: 12.0,
                        ),
                      ),
                    ),
                    Flex(
                      direction: Axis.horizontal,
                      children: [
                        Flexible(
                          flex: 1,
                          child: Container(
                              margin: EdgeInsets.only(left: 10.0, right: 2.0),
                              child: Divider()),
                        ),
                        Text(
                          "Already have an account",
                          style: TextStyle(
                              fontFamily: 'Roboto',
                              fontSize: 12.0,
                              color: Colors.black54),
                        ),
                        Flexible(
                          flex: 1,
                          child: Container(
                              margin: EdgeInsets.only(right: 10.0, left: 2.0),
                              child: Divider()),
                        ),
                      ],
                    ),
                    GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => SignIn()));
                      },
                      child: Container(
                        width: size.width,
                        margin:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                        height: 43,
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: 0.5,
                          ),
                          borderRadius: BorderRadius.circular(5.0),
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [Color(0xfff3f4f6), Color(0xffeaebef)]),
                        ),
                        child: Center(
                          child: Text(
                            "Sign-In now",
                            style: TextStyle(
                                fontSize: 14.0,
                                fontFamily: 'Roboto',
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      child: Divider(),
                      margin:
                          EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                    ),
                    Text(
                      '1996-2020 ,  inc. or its affiliates ',
                      style: TextStyle(
                        color: Colors.grey.shade500,
                        fontSize: 12.0,
                        fontFamily: 'Roboto',
                      ),
                    )
                  ],
                ),
              )
            : Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ),
      ),
    );
  }

  String validatePassword(String value) {
    String pattern = r'^[a-z0-9_-]{6,18}$';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return "Password is Required";
    } else if (!regExp.hasMatch(value)) {
      return "Invalid Password";
    } else if (value.length < 6) {
      return "Password must be more than 6";
    } else {
      return null;
    }
  }
}
