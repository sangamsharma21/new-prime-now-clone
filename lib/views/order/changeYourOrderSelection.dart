import 'package:flutter/material.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';
import 'package:prime_now_clone/views/YourAccountModule/manageAccount/updateNotification.dart';
import 'package:prime_now_clone/views/YourAccountModule/manageAccount/updatePayment.dart';
import 'package:prime_now_clone/views/YourAccountModule/manageAccount/updatePhoneNumber.dart';
import 'package:prime_now_clone/views/YourAccountModule/manageAccount/viewPastOrders.dart';
import 'package:prime_now_clone/returnAndRefunds/returnItem.dart';
import 'package:prime_now_clone/returnAndRefunds/returnPolicy.dart';

import 'cancelOrder.dart';
import 'changeOrModifyOrder.dart';

// import 'checkReturnStatus.dart';

class ChangeYourOrderSelection extends StatelessWidget {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: CustomAppbar(
          scaffoldKey: this.scaffoldKey,
        ),
      ),
      drawer: CustomDrawer(),
      backgroundColor: Colors.grey.shade300,
      body: Container(
        padding: EdgeInsets.all(10.0),
        child: Column(
          children: [
            SizedBox(
              height: 10.0,
            ),
            GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => ChangeModifyOrder()),
                );
              },
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(6.0),
                ),
                child: ListTile(
                  title: Text('Change or Modify Your Order'),
                  trailing: Icon(
                    Icons.arrow_forward_ios,
                    size: 17.0,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 2.0,
            ),
            GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => CancelOrder(),
                  ),
                );
              },
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(6.0),
                ),
                child: ListTile(
                  title: Text('Cancel Your Order'),
                  trailing: Icon(
                    Icons.arrow_forward_ios,
                    size: 17.0,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 2.0,
            ),
            // GestureDetector(
            //   onTap: (){
            //     Navigator.of(context).push(
            //       MaterialPageRoute(builder: (context) => CheckReturnStatus()),
            //     );
            //   },
            //   child: Container(
            //     decoration: BoxDecoration(
            //       color: Colors.white,
            //       borderRadius: BorderRadius.circular(6.0),
            //     ),
            //     child: ListTile(
            //       title: Text('Check Your Return Status'),
            //       trailing: Icon(Icons.arrow_forward_ios, size: 17.0,),
            //     ),
            //   ),
            // ),
            // SizedBox(
            //   height: 2.0,
            // ),
          ],
        ),
      ),
    );
  }
}
