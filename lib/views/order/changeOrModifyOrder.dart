import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';

class ChangeModifyOrder extends StatelessWidget{

  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context){
    return Scaffold(
      key: scaffoldKey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: CustomAppbar(scaffoldKey: this.scaffoldKey,),
      ),
      drawer: CustomDrawer(),
      backgroundColor: Colors.grey.shade300,
      body: Container(
        padding: EdgeInsets.all(15.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: Text('Change or Modify Your Order', style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 24.0,
                  color: Colors.black,
                ),
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Text(
                'Since we fulfill orders rapidly, it\'s not possible to change the delivery time, delivery address, or other order details after an order is placed. However, if you elect two-hour delivery it may be possible to cancel the order and place a new one as long as the order hasn\'t entered the delivery process.\n\n'
                    'To cancel an order:\n\n'
                    '1. Go to Your Orders.\n'
                    'In the app Your Orders can be accessed through the main menu.\n'
                    'On Primenow.com Your Orders can be found in Your Account menu.\n'
                    '2. Select the order you want to cancel.\n'
                    '3. It will display whether the order can be canceled\n'
                    '4. If it can be canceled, you\'ll see a Cancel Delivery button.\n'
                    'If you\'re unavailable during the delivery time that you selected and the courier is unable to deliver your order as a result, it will be returned for a full refund.\n', style: TextStyle(
                fontSize: 15.0,
              ),
                // textAlign: TextAlign.
              ),
              // RichText(
              //   text: TextSpan(
              //     children: [
              //       TextSpan(
              //           text: "Note: ", style: TextStyle(
              //         fontWeight: FontWeight.bold,
              //         color: Colors.black,
              //       )
              //       ),
              //       TextSpan(
              //           text: 'Please see our Return Policy for return time frames and information about what items can be returned. After the carrier has received your item, it can take up to 2 weeks to receive and process your return.', style: TextStyle(
              //           color: Colors.black
              //       )
              //       ),
              //     ],
              //   ),
              // ),
            ],
          ),
        ),
      ),
    );
  }
}