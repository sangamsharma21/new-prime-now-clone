import 'package:flutter/material.dart';
import 'package:prime_now_clone/Utils/colors.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';

class ManageVoiceRecordings extends StatefulWidget {
  @override
  _ManageVoiceRecordingsState createState() => _ManageVoiceRecordingsState();
}

class _ManageVoiceRecordingsState extends State<ManageVoiceRecordings> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: CustomDrawer(),
        key: scaffoldKey,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(kToolbarHeight),
            child: CustomAppbar(
              scaffoldKey: this.scaffoldKey,
            )),
        body: Container(
          width: MediaQuery.of(context).size.width,
          margin: EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                "Manage Voice Search",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
              ),
              Container(
                margin: EdgeInsets.only(top: 20.0),
                child: Text(
                  "When you use voice search  with the Prime Now App, we keep the voice recording associated with your account to learn how you speak to improve the accuracy of results provided to you and to improve our services.\n \n You can choose to delete voice recordings you've made in your Prime Now App That are associated with your account. This will delete these associated voice recordings you've made in the Prime Now App on all mobile devices and may degrade your experience using voice features.",
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400),
                ),
              ),
              SizedBox(
                height: 26,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.symmetric(vertical: 10),
                height: 43,
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 0.5,
                  ),
                  borderRadius: BorderRadius.circular(5.0),
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [CustomColors.lightYellow, CustomColors.yellow]),
                ),
                child: Center(
                  child: Text(
                    "Delete Voice Recordings",
                    style: TextStyle(
                        fontSize: 14.0,
                        fontFamily: 'Roboto',
                        fontWeight: FontWeight.w400),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
