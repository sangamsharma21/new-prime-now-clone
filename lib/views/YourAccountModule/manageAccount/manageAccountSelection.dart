import 'package:flutter/material.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';
import 'package:prime_now_clone/views/YourAccountModule/manageAccount/updateNotification.dart';
import 'package:prime_now_clone/views/YourAccountModule/manageAccount/updatePayment.dart';
import 'package:prime_now_clone/views/YourAccountModule/manageAccount/updatePhoneNumber.dart';
import 'package:prime_now_clone/views/YourAccountModule/manageAccount/viewPastOrders.dart';
import 'newDeliveryAddress.dart';

class ManageAccountSelection extends StatelessWidget {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: CustomAppbar(
          scaffoldKey: this.scaffoldKey,
        ),
      ),
      drawer: CustomDrawer(),
      backgroundColor: Colors.grey.shade300,
      body: Container(
        padding: EdgeInsets.all(10.0),
        child: Column(
          children: [
            SizedBox(
              height: 10.0,
            ),
            GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => NewDeliveryAddress()),
                );
              },
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(6.0),
                ),
                child: ListTile(
                  title: Text('Add a New Delivery Address'),
                  trailing: Icon(
                    Icons.arrow_forward_ios,
                    size: 17.0,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 2.0,
            ),
            GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => ViewPastOrders(),
                  ),
                );
              },
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(6.0),
                ),
                child: ListTile(
                  title: Text('View Past Orders'),
                  trailing: Icon(
                    Icons.arrow_forward_ios,
                    size: 17.0,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 2.0,
            ),
            GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => UpdateNotification()),
                );
              },
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(6.0),
                ),
                child: ListTile(
                  title: Text('Update Notification Preferences'),
                  trailing: Icon(
                    Icons.arrow_forward_ios,
                    size: 17.0,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 2.0,
            ),
            GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => UpdatePayment()),
                );
              },
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(6.0),
                ),
                child: ListTile(
                  title: Text('Update Payment Methods'),
                  trailing: Icon(
                    Icons.arrow_forward_ios,
                    size: 17.0,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 2.0,
            ),
            GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (_) => UpdatePhoneNumber()),
                );
              },
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(6.0),
                ),
                child: ListTile(
                  title: Text('Update Your Phone Number'),
                  trailing: Icon(
                    Icons.arrow_forward_ios,
                    size: 17.0,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
