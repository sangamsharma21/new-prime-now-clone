import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';

class ViewPastOrders extends StatelessWidget{

  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context){
    return Scaffold(
      key: scaffoldKey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: CustomAppbar(scaffoldKey: this.scaffoldKey,),
      ),
      drawer: CustomDrawer(),
      backgroundColor: Colors.grey.shade300,
      body: Container(
        padding: EdgeInsets.all(15.0),
        child: Column(
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Text('View Past Orders', style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 24.0,
                color: Colors.black,
              ),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              'You can view past orders in Your Orders.\n\n'
                  'To view your past orders:\n\n'
                  '1. Go to Your Orders.\n'
                  'In the app Your Orders can be accessed through the main menu.\n'
                  'On Primenow.com Your Orders can be found in the Your Account menu\n'
                  '2. To view the details of a particular order, select that order.\n', style: TextStyle(
              fontSize: 15.0,
            ),
              // textAlign: TextAlign.
            ),
          ],
        ),
      ),
    );
  }
}