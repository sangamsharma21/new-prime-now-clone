import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';

class UpdatePayment extends StatelessWidget{

  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context){
    return Scaffold(
      key: scaffoldKey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: CustomAppbar(scaffoldKey: this.scaffoldKey,),
      ),
      drawer: CustomDrawer(),
      backgroundColor: Colors.grey.shade300,
      body: Container(
        padding: EdgeInsets.all(15.0),
        child: Column(
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Text('Update Payment Methods', style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 24.0,
                color: Colors.black,
              ),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              'You can add, edit, or delete a payment method in Your Account in the app.\n\n'
                  'To manage your payment methods:\n\n'
                  '1. Go to Your Account.\n'
                  'In the app you can access Your Account through the main menu.\n'
                  'On Primenow.com there’s a Your Account menu in the main navigation bar.\n'
                  '2. Select Your Payment Methods.\n'
              , style: TextStyle(
              fontSize: 15.0,
            ),
              // textAlign: TextAlign.
            ),
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: 'Note: ',style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                  ),
                  TextSpan(
                    text: 'The following payment methods are accepted for Amazon Prime Now orders: Credit or debit cards (Visa, MasterCard, American Express, and Discover) and Amazon.com Gift Cards. Amazon.com Gift Cards can\'t be used to pay for tips, Amazon Restaurants items, or alcohol products.',style: TextStyle(
                    // fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
