import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';

class UpdatePhoneNumber extends StatelessWidget{

  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context){
    return Scaffold(
      key: scaffoldKey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: CustomAppbar(scaffoldKey: this.scaffoldKey,),
      ),
      drawer: CustomDrawer(),
      backgroundColor: Colors.grey.shade300,
      body: Container(
        padding: EdgeInsets.all(15.0),
        child: Column(
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Text('Update Your Mobile Number', style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 24.0,
                color: Colors.black,
              ),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              'You can update the mobile number used for delivery notifications in Your Account in the app.\n\n'
                  'To update your mobile number:\n\n'
                  '1. Go to Your Account.\n'
                  'In the app you can access Your Account through the main menu.\n'
                  'On Primenow.com there\’s a Your Account menu in the main navigation bar.\n'
                  '2. Select Your Mobile Number.\n', style: TextStyle(
              fontSize: 15.0,
            ),
              // textAlign: TextAlign.
            ),
          ],
        ),
      ),
    );
  }
}