import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';

class NewDeliveryAddress extends StatelessWidget{

  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context){
    return Scaffold(
      key: scaffoldKey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: CustomAppbar(scaffoldKey: this.scaffoldKey,),
      ),
      drawer: CustomDrawer(),
      backgroundColor: Colors.grey.shade300,
      body: Container(
        padding: EdgeInsets.all(15.0),
        child: Column(
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Text('Add a New Delivery Address', style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 24.0,
                color: Colors.black,
              ),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              'You can add a new delivery address in Your Account.\n\n'
              'To add a new delivery address:\n\n'
              '1. Go to Your Account.\n'
              'In the app you can access Your Account through the main menu.\n'
              'On Primenow.com there\’s a Your Account menu in the main navigation bar.\n'
              '2. Select Your Addresses.\n'
              '3. Select Add a New Address.', style: TextStyle(
              fontSize: 15.0,
            ),
              // textAlign: TextAlign.
            ),
          ],
        ),
      ),
    );
  }
}