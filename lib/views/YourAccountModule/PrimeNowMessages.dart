import 'package:flutter/material.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';

class PrimeNowMessages extends StatefulWidget {
  @override
  _PrimeNowMessagesState createState() => _PrimeNowMessagesState();
}

class _PrimeNowMessagesState extends State<PrimeNowMessages> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: CustomDrawer(),
        key: scaffoldKey,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(kToolbarHeight),
            child: CustomAppbar(
              scaffoldKey: this.scaffoldKey,
            )),
        body: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              Container(
                height: 80,
                width: 330,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    border: Border.all(color: Colors.black12)),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 14, right: 120),
                      child: Text(
                        "Select Period for Messages",
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text("All"),
                        SizedBox(
                          width: 230,
                        ),
                        Icon(Icons.arrow_forward_ios)
                      ],
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 105, top: 16),
                child: Text(
                  "You have no messages",
                  style: TextStyle(fontSize: 20),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
