import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';

class YourPaymentMethods extends StatefulWidget {
  @override
  _YourPaymentMethodsState createState() => _YourPaymentMethodsState();
}

class _YourPaymentMethodsState extends State<YourPaymentMethods> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  FocusNode _focusNode = new FocusNode();
  bool addingPaymentMethod = false;
  bool userNameCheckBox = false;

  addPaymentMethod() {
    setState(() {
      addingPaymentMethod = !addingPaymentMethod;
    });
  }

//extension NumberParsing on String{}
  @override
  void dispose() {
    // TODO: implement dispose
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: true,
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        drawer: CustomDrawer(),
        key: scaffoldKey,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(kToolbarHeight),
            child: CustomAppbar(
              scaffoldKey: this.scaffoldKey,
            )),
        body: addingPaymentMethod
            ? Container(
                child: Column(
                  children: [
                    Container(
                        margin: EdgeInsets.symmetric(horizontal: 10.0),
                        height: 40,
                        // width: 330,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          color: Colors.grey[100],
                          // border: Border.all(color: Colors.black12)
                        ),
                        child: FlatButton(
                          height: 20.0,
                          child: Row(
                            children: [
                              Icon(
                                Icons.arrow_back_ios,
                                size: 15,
                              ),
                              Expanded(
                                child: Text(
                                  "Back",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                          onPressed: () {
                            setState(() {
                              addingPaymentMethod = !addingPaymentMethod;
                            });
                          },
                        )),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ExpansionTile(
                        title: const Text(
                          "Add a credit or debit card",
                          style:
                              TextStyle(fontSize: 15.0, color: Colors.blueGrey),
                        ),
                        initiallyExpanded: true,
                        expandedCrossAxisAlignment: CrossAxisAlignment.start,
                        expandedAlignment: Alignment.topLeft,
                        children: [
                          const Text("Enter your credit card information"),
                          Row(
                            children: [
                              Checkbox(
                                value: userNameCheckBox,
                                onChanged: (value) {
                                  setState(() {
                                    userNameCheckBox = value;
                                  });
                                },
                              ),
                              const Text("Use name on Account")
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                margin: EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
              )
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 15.0, horizontal: 15.0),
                    child: Text(
                      "How would you like to pay?",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 15.0),
                    child: Row(
                      //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "! ",
                          style: TextStyle(
                              color: Colors.red,
                              fontSize: 18,
                              fontFamily: 'Roboto',
                              fontWeight: FontWeight.bold),
                        ),
//                    flex: 1,
//                  ),
                        Expanded(
                          child: Text(
                            " Please Select a default shipping address in an eligible ZIP code before changing your preferred payment method.",
                            style: TextStyle(
                              color: Colors.red,
                              fontSize: 15.0,
                            ),
                            //softWrap: true,
                            overflow: TextOverflow.clip,
                            maxLines: 3,
                          ),
                        ),

//                    flex: 8,
//                  ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 15.0),
                    child: Text(
                      "Wallet",
                      style:
                          TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                      margin: EdgeInsets.symmetric(horizontal: 15.0),
                      height: 40,
                      // width: 330,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          color: Colors.white,
                          border: Border.all(color: Colors.black12)),
                      child: FlatButton(
                        height: 20.0,
                        child: Row(
                          children: [
                            Expanded(
                              child: const Text(
                                "Add a Payment Method",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                            const Icon(
                              Icons.arrow_forward_ios_sharp,
                              size: 15,
                            )
                          ],
                        ),
                        onPressed: addPaymentMethod,
                      )),
                  SizedBox(
                    height: 40,
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 15.0),
                    height: 40,
                    // width: 330,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        color: Colors.white,
                        border: Border.all(color: Colors.black12)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        //Image.asset("Assets/device.jpg"),
                        Text(
                          "Amazon Gift Card",
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                        SizedBox(
                          width: 80,
                        ),
                        Text("0.00")
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    height: 140,
                    //width: 325,
                    margin: EdgeInsets.symmetric(horizontal: 15.0),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black12)),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 20, right: 190),
                          child: Text(
                            "Gift Cards",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 17),
                          ),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(right: 20),
                                child: Container(
                                  width: 160,
                                  margin: EdgeInsets.all(12),
                                  decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(4)),
                                      border: Border.all(color: Colors.black12),
                                      color: Colors.black12),
                                  child: TextField(
                                    style: TextStyle(color: Colors.black),
                                    focusNode: _focusNode,
                                    decoration: InputDecoration(
                                      contentPadding:
                                          EdgeInsets.symmetric(vertical: 15),
                                      border: InputBorder.none,
                                      hintText: '  Enter Code',
                                      // hintStyle: TextStyle(color: Color.fromRGBO(142, 142, 147, 1)),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(right: 15.0),
                              width: 100.0,
                              height: 40.0,
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.black54),
                                  borderRadius: BorderRadius.circular(4)),
                              child: RaisedButton(
                                child: Text('Apply'),
                                onPressed: () {},
                                color: Colors.white70,
                              ),
                            ),
                            // RaisedButton(
                            //   color: Colors.blue,
                            //   child: Text("Apply"),
                            //   onPressed: (){},
                            // )
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
      ),
    );
  }
}
