import 'package:flutter/material.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';

class NotificationSettings extends StatefulWidget {
  @override
  _NotificationSettingsState createState() => _NotificationSettingsState();
}

class _NotificationSettingsState extends State<NotificationSettings> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: CustomDrawer(),
        key: scaffoldKey,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(kToolbarHeight),
            child: CustomAppbar(
              scaffoldKey: this.scaffoldKey,
            )),
        body: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 15, right: 110),
                  child: Text(
                    "Notification Settings",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 16, bottom: 20, left: 22),
                  child: Text(
                      "By turning on Sms text messages, you consent to\nreceive automated text messages (including\nmarketing messages) from or on behalf of Amazon about Prime Now at your mobile number below.consent is not a condition of purchase. You can opt out at any time using the settings below. Message & data rates may apply. By turning on SMS text message you agree to Amazon's Condition of Use"),
                ),
                Container(
                  height: 50,
                  width: 330,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: Colors.white,
                      border: Border.all(color: Colors.black12)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text(
                        "Mobile Number",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        width: 120,
                      ),
                      Icon(
                        Icons.arrow_forward_ios,
                        size: 20,
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  height: 200,
                  width: 330,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      border: Border.all(color: Colors.black12)),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 12, top: 20),
                            child: Text(
                              "Delivery Status Updates",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16),
                            ),
                          ),
                          SizedBox(
                            width: 80,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                color: Colors.orange,
                              ),
                              width: 50,
                              height: 40,
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 10, top: 1, bottom: 1, right: 1),
                                child: RaisedButton(
                                  onPressed: () {},
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 70, top: 10),
                        child:
                            Text("Automates notifications about your \norder"),
                      ),
                      Divider(
                        thickness: 1,
                        indent: 10,
                        endIndent: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 10, bottom: 15, left: 12),
                        child: Row(
                          children: [
                            Text(
                              "SMS Text Message",
                              style: TextStyle(fontSize: 18),
                            ),
                            SizedBox(
                              width: 92,
                            ),
                            Icon(
                              Icons.adjust_sharp,
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 12),
                        child: Row(
                          children: [
                            Text(
                              "Push Notification",
                              style: TextStyle(fontSize: 18),
                            ),
                            SizedBox(
                              width: 110,
                            ),
                            Icon(
                              Icons.adjust_sharp,
                              color: Colors.orange,
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  width: 330,
                  height: 100,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      border: Border.all(color: Colors.black12)),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 12, top: 20),
                            child: Text(
                              "Alexa Shopping Updates",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16),
                            ),
                          ),
                          SizedBox(
                            width: 75,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                color: Colors.orange,
                              ),
                              width: 50,
                              height: 40,
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 10, top: 1, bottom: 1, right: 1),
                                child: RaisedButton(
                                  onPressed: () {},
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 90, top: 10),
                        child: Text("Notification while you shop with\nAlexa"),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  height: 190,
                  width: 330,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      border: Border.all(color: Colors.black12)),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 12, top: 20),
                            child: Text(
                              "Change to your orders",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16),
                            ),
                          ),
                          SizedBox(
                            width: 93,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                color: Colors.orange,
                              ),
                              width: 50,
                              height: 40,
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 10, top: 1, bottom: 1, right: 1),
                                child: RaisedButton(
                                  onPressed: () {},
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 55, top: 10),
                        child: Text("Messages from your in-store shopper"),
                      ),
                      Divider(
                        thickness: 1,
                        endIndent: 10,
                        indent: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 10, bottom: 16, left: 12),
                        child: Row(
                          children: [
                            Text(
                              "SMS Text Message",
                              style: TextStyle(fontSize: 18),
                            ),
                            SizedBox(
                              width: 92,
                            ),
                            Icon(
                              Icons.adjust_sharp,
                              color: Colors.orange,
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 12),
                        child: Row(
                          children: [
                            Text(
                              "Push Notification",
                              style: TextStyle(fontSize: 18),
                            ),
                            SizedBox(
                              width: 110,
                            ),
                            Icon(Icons.adjust_sharp)
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  width: 330,
                  height: 100,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      border: Border.all(color: Colors.black12)),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 12, top: 20),
                            child: Text(
                              "Personalized Notifications",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16),
                            ),
                          ),
                          SizedBox(
                            width: 60,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                color: Colors.orange,
                              ),
                              width: 50,
                              height: 40,
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 10, top: 1, bottom: 1, right: 1),
                                child: RaisedButton(
                                  onPressed: () {},
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 70, top: 10),
                        child: Text(
                            "Message about events, promotions,\nand other helpful reminders"),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 100,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
