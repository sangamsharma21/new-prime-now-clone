import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';
import 'package:prime_now_clone/views/YourAccountModule/ManageVoiceRecordings.dart';
import 'package:prime_now_clone/views/YourAccountModule/NotifictionSettings.dart';
import 'package:prime_now_clone/views/YourAccountModule/PrimeNowMessages.dart';
import 'package:prime_now_clone/views/YourAccountModule/YourAddresses.dart';
import 'package:prime_now_clone/views/YourAccountModule/YourMobileNumber.dart';
import 'package:prime_now_clone/views/YourAccountModule/YourPaymentMethods.dart';
import 'package:prime_now_clone/views/order/YourOrders.dart';

//import 'file:///E:/XcrinoProjects/prime_now_clone/lib/YourAccountModule/YourAddresses.dart';

class YourAccount extends StatefulWidget {
  @override
  _YourAccountState createState() => _YourAccountState();
}

class _YourAccountState extends State<YourAccount> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xfff3f3f3),
        drawer: CustomDrawer(),
        key: scaffoldKey,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(kToolbarHeight),
            child: CustomAppbar(
              scaffoldKey: this.scaffoldKey,
            )),
        body: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Container(
                  margin: EdgeInsets.only(left: 10.0, top: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Orders',
                        style: TextStyle(
                          fontFamily: 'Roboto',
                          color: Colors.black54,
                          fontWeight: FontWeight.w400,
                          fontSize: 18.0,
                        ),
                      ),
                    ],
                  ),
                ),
                GestureDetector(
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(3.0)),
                    margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Row(
                        children: [
                          Container(
                            child: Text(
                              'Your Orders',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14.0,
                                  fontFamily: 'Roboto',
                                  fontWeight: FontWeight.w500),
                            ),
                            margin: EdgeInsets.only(right: 10),
                          ),
                          Spacer(),
                          Icon(
                            Icons.arrow_forward_ios,
                            size: 18,
                            color: Colors.black54,
                          )
                        ],
                      ),
                    ),
                  ),
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => YourOrders()));
                  },
                ),
                Container(
                  margin: EdgeInsets.only(left: 10.0, top: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Account Settings',
                        style: TextStyle(
                          fontFamily: 'Roboto',
                          color: Colors.black54,
                          fontWeight: FontWeight.w400,
                          fontSize: 20.0,
                        ),
                      ),
                    ],
                  ),
                ),
                GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => YourAddresses()));
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(3.0)),
                    margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 10),
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Row(
                        children: [
                          Container(
                            child: Text(
                              'Your Addresses',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14.0,
                                  fontFamily: 'Roboto',
                                  fontWeight: FontWeight.w500),
                            ),
                            margin: EdgeInsets.only(right: 10),
                          ),
                          Spacer(),
                          Icon(
                            Icons.arrow_forward_ios,
                            size: 18,
                            color: Colors.black54,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(3.0)),
                    margin: EdgeInsets.symmetric(horizontal: 10, vertical: 1.0),
                    padding: EdgeInsets.all(0.0),
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Row(
                        children: [
                          Container(
                            child: Text(
                              'Your Payment Methods',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14.0,
                                  fontFamily: 'Roboto',
                                  fontWeight: FontWeight.w500),
                            ),
                            margin: EdgeInsets.only(right: 10),
                          ),
                          Spacer(),
                          Icon(
                            Icons.arrow_forward_ios,
                            size: 18,
                            color: Colors.black54,
                          )
                        ],
                      ),
                    ),
                  ),
                  behavior: HitTestBehavior.opaque,
                  onTap: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => YourPaymentMethods())),
                ),
                GestureDetector(
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(3.0)),
                    margin: EdgeInsets.symmetric(horizontal: 10, vertical: 1.0),
                    padding: EdgeInsets.all(0.0),
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Row(
                        children: [
                          Container(
                            child: Text(
                              'Your Mobile Number',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14.0,
                                  fontFamily: 'Roboto',
                                  fontWeight: FontWeight.w500),
                            ),
                            margin: EdgeInsets.only(right: 10),
                          ),
                          Spacer(),
                          Icon(
                            Icons.arrow_forward_ios,
                            size: 18,
                            color: Colors.black54,
                          )
                        ],
                      ),
                    ),
                  ),
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => YourMobileNumber()));
                  },
                ),
                GestureDetector(
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(3.0)),
                    margin: EdgeInsets.symmetric(horizontal: 10, vertical: 1.0),
                    padding: EdgeInsets.all(0.0),
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Row(
                        children: [
                          Container(
                            child: Text(
                              'Notification Settings',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14.0,
                                  fontFamily: 'Roboto',
                                  fontWeight: FontWeight.w500),
                            ),
                            margin: EdgeInsets.only(right: 10),
                          ),
                          Spacer(),
                          Icon(
                            Icons.arrow_forward_ios,
                            size: 18,
                            color: Colors.black54,
                          )
                        ],
                      ),
                    ),
                  ),
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => NotificationSettings()));
                  },
                ),
                Container(
                  margin: EdgeInsets.only(left: 10.0, top: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'App Preferences',
                        style: TextStyle(
                          fontFamily: 'Roboto',
                          color: Colors.black54,
                          fontWeight: FontWeight.w400,
                          fontSize: 18.0,
                        ),
                      ),
                    ],
                  ),
                ),
                GestureDetector(
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(3.0)),
                    margin:
                        EdgeInsets.symmetric(horizontal: 10, vertical: 10.0),
                    padding: EdgeInsets.all(0.0),
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Row(
                        children: [
                          Container(
                            child: Text(
                              'Manage Voice Recordings',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14.0,
                                  fontFamily: 'Roboto',
                                  fontWeight: FontWeight.w500),
                            ),
                            margin: EdgeInsets.only(right: 10),
                          ),
                          Spacer(),
                          Icon(
                            Icons.arrow_forward_ios,
                            size: 18,
                            color: Colors.black54,
                          )
                        ],
                      ),
                    ),
                  ),
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ManageVoiceRecordings()));
                  },
                ),
                Container(
                  margin: EdgeInsets.only(left: 10.0, top: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Message Center',
                        style: TextStyle(
                          fontFamily: 'Roboto',
                          color: Colors.black54,
                          fontWeight: FontWeight.w400,
                          fontSize: 18.0,
                        ),
                      ),
                    ],
                  ),
                ),
                GestureDetector(
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(3.0)),
                    margin:
                        EdgeInsets.symmetric(horizontal: 10, vertical: 10.0),
                    padding: EdgeInsets.all(0.0),
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Row(
                        children: [
                          Container(
                            child: Text(
                              'Prime Now Messages',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14.0,
                                  fontFamily: 'Roboto',
                                  fontWeight: FontWeight.w500),
                            ),
                            margin: EdgeInsets.only(right: 10),
                          ),
                          Spacer(),
                          Icon(
                            Icons.arrow_forward_ios,
                            size: 18,
                            color: Colors.black54,
                          )
                        ],
                      ),
                    ),
                  ),
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PrimeNowMessages()));
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
