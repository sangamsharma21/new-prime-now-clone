import 'package:flutter/material.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';
import 'package:prime_now_clone/views/YourAccountModule/YourAddresses.dart';

class YourMobileNumber extends StatefulWidget {
  @override
  _YourMobileNumberState createState() => _YourMobileNumberState();
}

class _YourMobileNumberState extends State<YourMobileNumber> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: CustomDrawer(),
        key: scaffoldKey,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(kToolbarHeight),
            child: CustomAppbar(
              scaffoldKey: this.scaffoldKey,
            )),
        body: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: [
                SizedBox(
                  height: 20,
                ),
                Container(
                  height: 120,
                  width: 330,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      border: Border.all(color: Colors.red)),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 240, top: 10),
                        child: Text(
                          "Error",
                          style: TextStyle(
                              color: Colors.red,
                              fontWeight: FontWeight.bold,
                              fontSize: 18),
                        ),
                      ),
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 20, left: 5),
                            child: Icon(
                              Icons.adjust_sharp,
                              size: 10,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 16, top: 15),
                            child: Text(
                              "No default delivery address present,\n please add a delivery address to set your\ncontact number",
                              style: TextStyle(fontWeight: FontWeight.w500),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 30, right: 70),
                  child: Text(
                    "Where would you like to\nreceive your deliveries?",
                    style: TextStyle(fontSize: 22, fontWeight: FontWeight.w500),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 30, right: 11),
                  child: Text(
                    "You do not have any saved addresses in the\nbotson delivery area.",
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                GestureDetector(
                  child: Container(
                    height: 50,
                    width: 330,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        color: Colors.white,
                        border: Border.all(color: Colors.black54)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text("Add a new address"),
                        SizedBox(
                          width: 120,
                        ),
                        Icon(
                          Icons.arrow_forward_ios,
                          size: 15,
                        )
                      ],
                    ),
                  ),
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => YourAddresses()));
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
