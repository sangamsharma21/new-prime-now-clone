import 'package:flutter/material.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';

class AddATip extends StatefulWidget {
  @override
  _AddATipState createState() => _AddATipState();
}

class _AddATipState extends State<AddATip> {
  var text =
      'To change the tip amount on a particular order, select Tip on the final page before you place the order.'
      ' You can also change your tip for orders other than Amazon Restaurants orders up to 24 hours after delivery.'
      ' To update your tip amount:'
      'Open up the order summary in Your Orders.'
      ' In the app Your Orders can be accessed through the main menu.'
      ' On Primenow.com Your Orders can be found in the Your Account menu.'
      'Select Edit your tip.'
      'Enter the new tip amount.'
      'Select Change Tip.';

  var text2 =
      'Tipping is completely optional. If you elect to leave a tip, the entire tip goes to your courier.'
      ' Information about your tip, such as the amount or who has issued it, isn\'t shared.';

  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      drawer: CustomDrawer(),
      key: scaffoldKey,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(kToolbarHeight),
          child: CustomAppbar(
            scaffoldKey: this.scaffoldKey,
          )),
      backgroundColor: Color(0xfff3f3f3),
      body: Container(
        height: size.height,
        width: size.width,
        child: Column(
          children: [
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Add a Tip',
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Roboto',
                      fontSize: 24.0,
                    ),
                  ),
                ],
              ),
              margin: EdgeInsets.fromLTRB(10, 30, 10, 0),
            ),
            Container(
              child: Text(
                text,
                style: TextStyle(
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.w300,
                    color: Colors.black54,
                    fontSize: 14.0),
              ),
              margin: EdgeInsets.all(10.0),
            ),
            Container(
              margin: EdgeInsets.all(10.0),
              child: RichText(
                text: TextSpan(children: [
                  TextSpan(
                      text: 'Note:',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Roboto',
                          fontSize: 14.0)),
                  TextSpan(
                      text: text2,
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.normal,
                          fontFamily: 'Roboto',
                          fontSize: 14.0)),
                ]),
              ),
            )
          ],
        ),
      ),
    );
  }
}
