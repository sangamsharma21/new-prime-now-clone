import 'package:flutter/material.dart';
import 'package:prime_now_clone/views/HelpSectionModule/AddSpecialDeliveryInstructions.dart';
import 'package:prime_now_clone/views/HelpSectionModule/AddaTip.dart';
import 'package:prime_now_clone/views/HelpSectionModule/ChooseDeliveryAddress.dart';
import 'package:prime_now_clone/views/HelpSectionModule/ChooseDeliveryWindow.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';

class PlaceYourOrder extends StatefulWidget {
  @override
  _PlaceYourOrderState createState() => _PlaceYourOrderState();
}

class _PlaceYourOrderState extends State<PlaceYourOrder> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Color(0xfff3f3f3),
      drawer: CustomDrawer(),
      key: scaffoldKey,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(kToolbarHeight),
          child: CustomAppbar(
            scaffoldKey: this.scaffoldKey,
          )),
      body: Container(
        height: size.height,
        width: size.width,
        margin: EdgeInsets.only(top: 20),
        child: Column(
          children: [
            GestureDetector(
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(3.0)),
                margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 10),
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Row(
                    children: [
                      Container(
                        child: Text(
                          'Choose Your Delivery Window',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14.0,
                              fontFamily: 'Roboto',
                              fontWeight: FontWeight.w400),
                        ),
                        margin: EdgeInsets.only(right: 10),
                      ),
                      Spacer(),
                      Icon(
                        Icons.arrow_forward_ios,
                        size: 18,
                        color: Colors.black54,
                      )
                    ],
                  ),
                ),
              ),
              behavior: HitTestBehavior.opaque,
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ChooseDeliveryWindow()));
              },
            ),
            GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ChooseDeliveryAddress())),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(3.0)),
                margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 2.0),
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Row(
                    children: [
                      Container(
                        child: Text(
                          'Choose a Delivery Address',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14.0,
                              fontFamily: 'Roboto',
                              fontWeight: FontWeight.w400),
                        ),
                        margin: EdgeInsets.only(right: 10),
                      ),
                      Spacer(),
                      Icon(
                        Icons.arrow_forward_ios,
                        size: 18,
                        color: Colors.black54,
                      )
                    ],
                  ),
                ),
              ),
            ),
            GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            AddSpecialDeliveryInstructions()));
              },
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(3.0)),
                margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 2.0),
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Row(
                    children: [
                      Container(
                        child: Text(
                          'Add Special Delivery Instructions ',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14.0,
                              fontFamily: 'Roboto',
                              fontWeight: FontWeight.w400),
                        ),
                        margin: EdgeInsets.only(right: 10),
                      ),
                      Spacer(),
                      Icon(
                        Icons.arrow_forward_ios,
                        size: 18,
                        color: Colors.black54,
                      )
                    ],
                  ),
                ),
              ),
            ),
            GestureDetector(
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(3.0)),
                margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 2.0),
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Row(
                    children: [
                      Container(
                        child: Text(
                          'Add a Tip',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14.0,
                              fontFamily: 'Roboto',
                              fontWeight: FontWeight.w400),
                        ),
                        margin: EdgeInsets.only(right: 10),
                      ),
                      Spacer(),
                      Icon(
                        Icons.arrow_forward_ios,
                        size: 18,
                        color: Colors.black54,
                      )
                    ],
                  ),
                ),
              ),
              behavior: HitTestBehavior.opaque,
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AddATip()));
              },
            ),
          ],
        ),
      ),
    );
  }
}
