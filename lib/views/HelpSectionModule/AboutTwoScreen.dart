import 'package:flutter/material.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';

class AboutTwoScreen extends StatefulWidget {
  @override
  _AboutTwoScreenState createState() => _AboutTwoScreenState();
}

class _AboutTwoScreenState extends State<AboutTwoScreen> {
  var text =
      'Amazon Prime Now is a benefit of Amazon Prime that allows members to place orders for fast, same-day delivery.'
      'Depending on your location, there are three delivery options for Amazon Prime Now orders: delivery in a two-hour window, '
      ' delivery in a one-hour window, and delivery within an hour. Select locations also have the option for eligible Prime members to pick up Prime Now orders at Whole Foods Market stores.'
      'Delivery within an hour and one-hour delivery windows are available in some ZIP Codes serviced by Amazon Prime Now. Two-hour delivery windows are available in all our service areas.'
      'Delivery fees vary depending on the delivery method you choose and the seller you’re ordering from and will be displayed when you place your order.'
      'FREE Ultrafast Delivery is available for two-hour delivery windows if your order includes at least  of eligible items. Some local sellers may have higher order thresholds to receive a free two-hour delivery window. In some regions, fees may apply to certain two-hour delivery windows even if you meet the order threshold. Additional delivery fees vary depending on the delivery method you choose and the seller you\'re ordering from and will be displayed when you place your order'
      'In addition to the tens of thousands of daily essentials available on Amazon Prime Now, you can shop from local stores in select cities.'
      ' If other stores are available in your city, you’ll see the option to browse them on the main page of the app and website. Prime members receive exclusive savings on eligible items from Whole Foods Market stores in select cities. The prices in the Prime Now app and website will automatically reflect the discounted price.'
      'You can also build a shopping cart with Alexa from Whole Foods Market for free 2-hour delivery through Prime Now. You can find more information here.'
      ' Once you meet the order minimum and your order is placed you\'ll receive tracking updates via text message. '
      'You can also view your order on its way to you using a map in Your Orders in Your Account.';
  var text2 = 'What is Amazon Prime Now and how does it work?';
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      drawer: CustomDrawer(),
      key: scaffoldKey,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(kToolbarHeight),
          child: CustomAppbar(
            scaffoldKey: this.scaffoldKey,
          )),
      body: SingleChildScrollView(
        child: Container(
          width: size.width,
          child: Column(
            children: [
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'About Amazon Prime Now',
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Roboto',
                        fontSize: 24.0,
                      ),
                    ),
                  ],
                ),
                margin: EdgeInsets.fromLTRB(10, 30, 10, 0),
              ),
              Container(
                child: Text(
                  text2,
                  style: TextStyle(
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.w300,
                      color: Colors.black54,
                      fontSize: 14.0),
                ),
                margin: EdgeInsets.all(10.0),
              ),
              Container(
                margin: EdgeInsets.all(10.0),
                child: Text(
                  text,
                  style: TextStyle(
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.w300,
                      color: Colors.black54,
                      fontSize: 14.0),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
