import 'package:flutter/material.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';

class ChooseDeliveryWindow extends StatefulWidget {
  @override
  _ChooseDeliveryWindowState createState() => _ChooseDeliveryWindowState();
}

class _ChooseDeliveryWindowState extends State<ChooseDeliveryWindow> {
  var text =
      'You\'ll be prompted to pick a two-hour delivery window, one-hour delivery window, or delivery within one hour during checkout for'
      ' Amazon and seller-fulfilled orders'
      ' For one-hour and two-hour delivery windows, just select the slot you want during checkout to reserve it for your order. If a delivery window '
      'doesn\'t appear, we don\'t have that time slot available at the moment';
  var text2 =
      'Delivery within an hour and one-hour delivery windows are available in most ZIP codes serviced by Amazon Prime Now'
      ' The delivery hours for each local store will be listed when viewing that store in the app or on the website';
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Color(0xfff3f3f3),
      drawer: CustomDrawer(),
      key: scaffoldKey,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(kToolbarHeight),
          child: CustomAppbar(
            scaffoldKey: this.scaffoldKey,
          )),
      body: Container(
        height: size.height,
        width: size.width,
        child: Column(
          children: [
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Choose Your Delivery Window',
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Roboto',
                      fontSize: 24.0,
                    ),
                  ),
                ],
              ),
              margin: EdgeInsets.fromLTRB(10, 30, 10, 0),
            ),
            Container(
              child: Text(
                text,
                style: TextStyle(
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.w300,
                    color: Colors.black54,
                    fontSize: 14.0),
              ),
              margin: EdgeInsets.all(10.0),
            ),
            Container(
              margin: EdgeInsets.all(10.0),
              child: RichText(
                text: TextSpan(children: [
                  TextSpan(
                      text: 'Note:',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Roboto',
                          fontSize: 14.0)),
                  TextSpan(
                      text: text2,
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.normal,
                          fontFamily: 'Roboto',
                          fontSize: 14.0)),
                ]),
              ),
            )
          ],
        ),
      ),
    );
  }
}
