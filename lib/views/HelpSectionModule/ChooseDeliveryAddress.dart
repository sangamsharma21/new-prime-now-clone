import 'package:flutter/material.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';

class ChooseDeliveryAddress extends StatefulWidget {
  @override
  _ChooseDeliveryAddressState createState() => _ChooseDeliveryAddressState();
}

class _ChooseDeliveryAddressState extends State<ChooseDeliveryAddress> {
  var text =
      'When you place your first order you\'ll be asked to confirm the address where you\'d like to receive your deliveries'
      'To update your default delivery address:'
      'Go to Your Account.'
      'In the app you can access Your Account through the main menu.'
      'On Primenow.com there\’s a Your Account menu in the main navigation bar.'
      'Select Your Addresses.'
      'Select the circle next to the address you\'d like to set as your default delivery address.';
  var text2 =
      ' If you ship orders to multiple addresses, you can change the shipping address for a particular order '
      'by selecting Delivery address on the final page before you place the order.';
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      drawer: CustomDrawer(),
      key: scaffoldKey,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(kToolbarHeight),
          child: CustomAppbar(
            scaffoldKey: this.scaffoldKey,
          )),
      body: Container(
        height: size.height,
        width: size.width,
        child: Column(
          children: [
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Choose Your Delivery Address',
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Roboto',
                      fontSize: 24.0,
                    ),
                  ),
                ],
              ),
              margin: EdgeInsets.fromLTRB(10, 30, 10, 0),
            ),
            Container(
              child: Text(
                text,
                style: TextStyle(
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.w300,
                    color: Colors.black54,
                    fontSize: 14.0),
              ),
              margin: EdgeInsets.all(10.0),
            ),
            Container(
              margin: EdgeInsets.all(10.0),
              child: RichText(
                text: TextSpan(children: [
                  TextSpan(
                      text: 'Note:',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Roboto',
                          fontSize: 14.0)),
                  TextSpan(
                      text: text2,
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.normal,
                          fontFamily: 'Roboto',
                          fontSize: 14.0)),
                ]),
              ),
            )
          ],
        ),
      ),
    );
  }
}
