import 'package:flutter/material.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';

class AddSpecialDeliveryInstructions extends StatefulWidget {
  @override
  _AddSpecialDeliveryInstructionsState createState() =>
      _AddSpecialDeliveryInstructionsState();
}

class _AddSpecialDeliveryInstructionsState
    extends State<AddSpecialDeliveryInstructions> {
  var text =
      'During checkout, you\’ll be able to add special delivery instructions for your delivery. '
      'Special instructions include things like building access codes or what doorbell to ring.'
      'To update the special delivery instructions on your order, select Delivery Instructions on the final page before placing your order.';

  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      drawer: CustomDrawer(),
      key: scaffoldKey,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(kToolbarHeight),
          child: CustomAppbar(
            scaffoldKey: this.scaffoldKey,
          )),
      body: Container(
        height: size.height,
        width: size.width,
        child: Column(
          children: [
            Container(
              child: Text(
                'Add Special Delivery Instructions ',
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Roboto',
                  fontSize: 24.0,
                ),
              ),
              margin: EdgeInsets.fromLTRB(10, 30, 10, 0),
            ),
            Container(
              child: Text(
                text,
                style: TextStyle(
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.w300,
                    color: Colors.black54,
                    fontSize: 14.0),
              ),
              margin: EdgeInsets.all(10.0),
            ),
          ],
        ),
      ),
    );
  }
}
