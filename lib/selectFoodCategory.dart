import 'package:flutter/material.dart';

import 'AllDepartmentsList.dart';
import 'ProductList.dart';
import 'Utils/colors.dart';

class SelectFoodCategory extends StatefulWidget {
  @override
  SelectFoodCategoryState createState() => SelectFoodCategoryState();
}

class SelectFoodCategoryState extends State<SelectFoodCategory> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: Container(
          color: CustomColors.darkGrey,
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              DrawerHeader(
                  padding: EdgeInsets.zero,
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          height: 50.0,
                          child: Image(
                            image: AssetImage("assets/images/logo.png"),
                          ),
                        ),
                        Center(
                          child: Container(
                            margin: EdgeInsets.only(top: 20.0),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  "Shopping in ",
                                  style: TextStyle(
                                      color: CustomColors.orange,
                                      fontWeight: FontWeight.w700,
                                      fontSize: 16),
                                ),
                                Text(
                                  "45014 ",
                                  style: TextStyle(
                                      color: CustomColors.orange,
                                      fontWeight: FontWeight.w700,
                                      fontSize: 16),
                                ),
                                Icon(
                                  Icons.location_pin,
                                  color: CustomColors.lightGrey,
                                  size: 16,
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  )),
              GestureDetector(
                onTap: () {},
                child: ListTile(
                  title: Text(
                    "Home",
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.3,
                child: Divider(
                  height: 1.0,
                  color: CustomColors.white,
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: ListTile(
                  title: Text(
                    "Shop Past Purchases",
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.3,
                child: Divider(
                  height: 1.0,
                  color: CustomColors.white,
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AllDepartmentList()));
                },
                child: ListTile(
                  title: Text(
                    "Shop by Department",
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.3,
                child: Divider(
                  height: 1.0,
                  color: CustomColors.white,
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: ListTile(
                  title: Text(
                    "Your Orders",
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.3,
                child: Divider(
                  height: 1.0,
                  color: CustomColors.white,
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: ListTile(
                  title: Text(
                    "Your Account",
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.3,
                child: Divider(
                  height: 1.0,
                  color: CustomColors.white,
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: ListTile(
                  title: Text(
                    "Help & About",
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.3,
                child: Divider(
                  height: 1.0,
                  color: CustomColors.white,
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: ListTile(
                  title: Text(
                    "Contact Us",
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.3,
                child: Divider(
                  height: 1.0,
                  color: CustomColors.white,
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: ListTile(
                  title: Text(
                    "Change Location",
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.3,
                child: Divider(
                  height: 1.0,
                  color: CustomColors.white,
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: ListTile(
                  title: Text(
                    "Sign Out",
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.3,
                child: Divider(
                  height: 1.0,
                  color: CustomColors.white,
                ),
              ),
              ListTile()
            ],
          ),
        ),
      ),
      key: scaffoldKey,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
        leading: GestureDetector(
            onTap: () {
              scaffoldKey.currentState.openDrawer();
            },
            child: Icon(Icons.menu)),
        centerTitle: true,
        title: Container(
          width: MediaQuery.of(context).size.width,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 0.6,
                child: Center(
                  child: Container(
                      height: 35.0,
                      child:
                          Image(image: AssetImage("assets/images/logo.png"))),
                ),
              ),
              Icon(
                Icons.shopping_cart_outlined,
                color: Theme.of(context).primaryColor,
              )
            ],
          ),
        ),
      ),
      body: Column(
        children: [
          SizedBox(
            height: 10.0,
          ),
          ListTile(
            leading: Text(
              'Food Title',
              style: TextStyle(fontSize: 24.0, color: Colors.black),
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          Expanded(
            child: ListView.separated(
                itemCount: 6,
                separatorBuilder: (context, int i) {
                  return Divider(
                    color: Colors.grey,
                  );
                },
                itemBuilder: (context, int i) {
                  return ListTile(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (_) => ProductList(),
                      ));
                    },
                    leading: Text(
                      'Category $i',
                      style: TextStyle(
                        color: Colors.black,
                      ),
                    ),
                    trailing: Icon(Icons.arrow_forward_ios),
                  );
                }),
          ),
        ],
      ),
    );
  }
}

///ProductList
