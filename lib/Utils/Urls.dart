class Urls {
  static const String DomainUrl = 'https://oceansaks.com/';
  static const String Register = DomainUrl + 'api/user/register';
  static const String Verify = DomainUrl + 'api/user/verify-otp';
  static const String Login = DomainUrl + 'api/user/login';
  static const String AddAddress = DomainUrl + 'api/user/add-address';
  static const String getStore = DomainUrl + 'api/fetch-store';
  static const String addProductToCart = DomainUrl + 'api/add-to-cart';
  static const String getCart = DomainUrl + 'api/fetch-cart-data';
  static const String updateCart = DomainUrl + 'api/update-cart';
  static const String deleteCart = DomainUrl + 'api/remove-cart';
  static const String getProductDetail =
      DomainUrl + 'api/fetch-product-detail/';
  static const String bestSellarProducts =
      DomainUrl + 'api/fetch-best-seller-products';
  static const String getAllCategorySubCategory =
      DomainUrl + 'api/fetch-all-categories-and-subcategories';
  static const String getCategoryByStore =
      DomainUrl + 'api/fetch-all-categories-and-subcategories-by-store';
}
