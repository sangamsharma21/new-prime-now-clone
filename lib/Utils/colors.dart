import 'package:flutter/material.dart';

class CustomColors {
  static const Color darkGrey = Color(0xFF232f3f);
  static const Color lightGrey = Color(0xFF3d4854);
  static const Color grey = Color(0xff6c6d6d);
  static const Color orange = Color(0xFFf59046);
  static const Color offWhite = Color(0xFFf2f2f2);
  static const Color darkOffWhite = Color(0xffe8edf1);
  static const Color white = Colors.white;
  static const Color blue = Color(0xFF189bd2);
  static const Color lightYellow = Color(0xFFf9e7b5);
  static const Color yellow = Color(0xFFf2d06a);
  static const Color red = Color(0xFFbc1d07);
  static const Color primeNowOrange = Color(0xFFff7817);
}
