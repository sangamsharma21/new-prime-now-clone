import 'package:flutter/material.dart';
import 'package:prime_now_clone/AllDepartmentsList.dart';
import 'package:prime_now_clone/ChangeLocation.dart';
import 'package:prime_now_clone/ContactUs.dart';
import 'package:prime_now_clone/DashboardScreen.dart';
import 'package:prime_now_clone/ShopPastPurchase.dart';
import 'package:prime_now_clone/views/HelpSectionModule/HelpSection.dart';
import 'package:prime_now_clone/views/YourAccountModule/YourAccount.dart';
import 'package:prime_now_clone/views/order/YourOrders.dart';

import 'colors.dart';

class CustomDrawer extends StatefulWidget {
  @override
  CustomDrawerState createState() => CustomDrawerState();
}

class CustomDrawerState extends State<CustomDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: CustomColors.darkGrey,
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
                padding: EdgeInsets.zero,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        height: 50.0,
                        child: Image(
                          image: AssetImage("assets/images/logo.png"),
                        ),
                      ),
                      Center(
                        child: GestureDetector(
                          behavior: HitTestBehavior.opaque,
                          onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ChangeLocation())),
                          child: Container(
                            margin: EdgeInsets.only(top: 20.0),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  "Shopping in ",
                                  style: TextStyle(
                                      color: CustomColors.orange,
                                      fontWeight: FontWeight.w700,
                                      fontSize: 16),
                                ),
                                Text(
                                  "45014 ",
                                  style: TextStyle(
                                      color: CustomColors.orange,
                                      fontWeight: FontWeight.w700,
                                      fontSize: 16),
                                ),
                                Icon(
                                  Icons.location_pin,
                                  color: CustomColors.lightGrey,
                                  size: 16,
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )),
            GestureDetector(
              onTap: () => Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => DashboardScreen())),
              child: ListTile(
                title: Text(
                  "Home",
                  style: Theme.of(context).textTheme.headline2,
                ),
              ),
            ),
            Opacity(
              opacity: 0.3,
              child: Divider(
                height: 1.0,
                color: CustomColors.white,
              ),
            ),
            GestureDetector(
              onTap: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ShopPastPurchase())),
              child: ListTile(
                title: Text(
                  "Shop Past Purchases",
                  style: Theme.of(context).textTheme.headline2,
                ),
              ),
            ),
            Opacity(
              opacity: 0.3,
              child: Divider(
                height: 1.0,
                color: CustomColors.white,
              ),
            ),
            GestureDetector(
              onTap: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => AllDepartmentList())),
              child: ListTile(
                title: Text(
                  "Shop by Department",
                  style: Theme.of(context).textTheme.headline2,
                ),
              ),
            ),
            Opacity(
              opacity: 0.3,
              child: Divider(
                height: 1.0,
                color: CustomColors.white,
              ),
            ),
            GestureDetector(
              onTap: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => YourOrders())),
              child: ListTile(
                title: Text(
                  "Your Orders",
                  style: Theme.of(context).textTheme.headline2,
                ),
              ),
            ),
            Opacity(
              opacity: 0.3,
              child: Divider(
                height: 1.0,
                color: CustomColors.white,
              ),
            ),
            GestureDetector(
              onTap: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => YourAccount())),
              child: ListTile(
                title: Text(
                  "Your Account",
                  style: Theme.of(context).textTheme.headline2,
                ),
              ),
            ),
            Opacity(
              opacity: 0.3,
              child: Divider(
                height: 1.0,
                color: CustomColors.white,
              ),
            ),
            GestureDetector(
              onTap: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => HelpSection())),
              child: ListTile(
                title: Text(
                  "Help & About",
                  style: Theme.of(context).textTheme.headline2,
                ),
              ),
            ),
            Opacity(
              opacity: 0.3,
              child: Divider(
                height: 1.0,
                color: CustomColors.white,
              ),
            ),
            GestureDetector(
              onTap: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ContactUs())),
              child: ListTile(
                title: Text(
                  "Contact Us",
                  style: Theme.of(context).textTheme.headline2,
                ),
              ),
            ),
            Opacity(
              opacity: 0.3,
              child: Divider(
                height: 1.0,
                color: CustomColors.white,
              ),
            ),
            GestureDetector(
              onTap: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ChangeLocation())),
              child: ListTile(
                title: Text(
                  "Change Location",
                  style: Theme.of(context).textTheme.headline2,
                ),
              ),
            ),
            Opacity(
              opacity: 0.3,
              child: Divider(
                height: 1.0,
                color: CustomColors.white,
              ),
            ),
            GestureDetector(
              onTap: () {
                signOut(context);
              },
              child: ListTile(
                title: Text(
                  "Sign Out",
                  style: Theme.of(context).textTheme.headline2,
                ),
              ),
            ),
            Opacity(
              opacity: 0.3,
              child: Divider(
                height: 1.0,
                color: CustomColors.white,
              ),
            ),
            ListTile()
          ],
        ),
      ),
    );
  }

  signOut(BuildContext context) {
    return showDialog(
        context: context,
        child: AlertDialog(
          title: Text(
            'Confirm Sign Out',
            style: TextStyle(
                fontSize: 16.0,
                color: Colors.black,
                fontWeight: FontWeight.bold),
          ),
          actions: [
            Container(
              child: Text(
                'you are signing out of your Amazon apps on this device.',
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  color: Colors.black54,
                  fontSize: 14.0,
                  fontFamily: 'Roboto',
                ),
              ),
              margin: EdgeInsets.symmetric(
                horizontal: 10,
              ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      margin: EdgeInsets.only(right: 5),
                      child: Text(
                        'Cancel',
                        style: TextStyle(
                            fontFamily: 'Roboto',
                            fontSize: 12.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.black),
                      ),
                    ),
                  ),
                  GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      margin: EdgeInsets.only(left: 5.0),
                      child: Text(
                        'Confirm',
                        style: TextStyle(
                            fontFamily: 'Roboto',
                            fontSize: 12.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.black),
                      ),
                    ),
                  ),
                ],
              ),
              margin: EdgeInsets.only(
                top: 20,
                bottom: 20,
                right: 20,
              ),
            )
          ],
        ));
  }
}
