import 'package:flutter/material.dart';
import 'package:prime_now_clone/Cart.dart';

class CustomAppbar extends StatefulWidget {
  CustomAppbar({this.scaffoldKey});

  final GlobalKey<ScaffoldState> scaffoldKey;

  @override
  CustomAppbarState createState() =>
      CustomAppbarState(scaffoldKey: this.scaffoldKey);
}

class CustomAppbarState extends State<CustomAppbar> {
  CustomAppbarState({this.scaffoldKey});

  final GlobalKey<ScaffoldState> scaffoldKey;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      brightness: Brightness.light,
      backgroundColor: Theme.of(context).backgroundColor,
      elevation: 0,
      leading: IconButton(
        onPressed: () => scaffoldKey.currentState.openDrawer(),
        icon: Icon(
          Icons.menu,
          color: Theme.of(context).primaryColor,
        ),
      ),
      title: Container(
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: MediaQuery.of(context).size.width * 0.6,
              child: Center(
                child: Container(
                    height: 35.0,
                    child: Image(image: AssetImage("assets/images/logo.png"))),
              ),
            ),
            GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () => Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Cart())),
              child: Icon(
                Icons.shopping_cart_outlined,
                color: Theme.of(context).primaryColor,
              ),
            )
          ],
        ),
      ),
    );
  }
}
