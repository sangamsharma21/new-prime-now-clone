import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:prime_now_clone/ProductDetail.dart';
import 'package:prime_now_clone/Utils/colors.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';

import 'Utils/customAppbar.dart';

class ProductList extends StatefulWidget {
  @override
  _ProductListState createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool showFilterExpansionTile = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: CustomDrawer(),
        key: scaffoldKey,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(kToolbarHeight),
            child: CustomAppbar(
              scaffoldKey: this.scaffoldKey,
            )),
        // drawer: CustomDrawer(),
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Stack(
            children: [
              Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Container(
                    child: Row(
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(15.0, 10.0, 1.0, 10.0),
                          width: MediaQuery.of(context).size.width * 0.6,
                          height: 55.0,
                          decoration: BoxDecoration(
                            color: CustomColors.darkOffWhite,
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          child: Row(
                            children: [
                              Expanded(
                                child: TextFormField(
                                  decoration: InputDecoration(
                                    prefixIcon: Icon(Icons.search),
                                    labelText: "Search",
                                    border: InputBorder.none,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(20.0, 10.0, 5.0, 10.0),
                          width: MediaQuery.of(context).size.width * 0.25,
                          child: Row(
                            children: [
                              GestureDetector(
                                child: Row(
                                  children: [
                                    Text('Filters'),
                                    Icon(Icons.keyboard_arrow_down_rounded),
                                  ],
                                ),
                                onTap: () {
                                  if (showFilterExpansionTile) {
                                    setState(() {
                                      showFilterExpansionTile =
                                          !showFilterExpansionTile;
                                    });
                                    print(showFilterExpansionTile);
                                  } else {
                                    setState(() {
                                      showFilterExpansionTile =
                                          !showFilterExpansionTile;
                                    });
                                  }
                                },
                              ),
                              // PopupMenuButton(
                              //   color: Colors.white,
                              //   icon: Icon(Icons.keyboard_arrow_down_rounded),
                              //   itemBuilder: (context){
                              //     return <PopupMenuItem>[
                              //       PopupMenuItem(
                              //           child: ExpansionTile(
                              //             title: Text('Departments'),
                              //
                              //             children: [
                              //               CheckboxListTile(
                              //                   title: Text('Grocery & Gourmet Food'),
                              //                 selected: false,
                              //                 value: false,
                              //               ),
                              //               CheckboxListTile(title: Text('Produce'), value: false,),
                              //               Text('Dried Fruits & Vegetables')
                              //             ],
                              //           ),
                              //       ),
                              //     ];
                              //   },
                              // ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 2.0),
                    height: 1,
                    color: CustomColors.darkOffWhite,
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 5.0),
                    child: Row(
                      children: [
                        Text(
                          "Shopping in ",
                          style: TextStyle(
                            fontSize: 14.0,
                            color: CustomColors.lightGrey,
                          ),
                        ),
                        Text(
                          "45014",
                          style: TextStyle(
                            fontSize: 14.0,
                            color: CustomColors.lightGrey,
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 2.0, bottom: 2.0),
                    height: 1,
                    color: CustomColors.darkOffWhite,
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 5.0),
                    child: Row(
                      children: [
                        Text(
                          "988 ",
                          style: TextStyle(
                            fontSize: 14.0,
                            color: CustomColors.grey,
                          ),
                        ),
                        Text(
                          "Results",
                          style: TextStyle(
                            fontSize: 14.0,
                            color: CustomColors.grey,
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 2.0),
                    height: 1,
                    color: CustomColors.darkOffWhite,
                  ),
                  Expanded(
                    child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: 10,
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ProductDetail()));
                            },
                            child: Container(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Container(
                                    height: 1.0,
                                    color: CustomColors.darkOffWhite,
                                  ),
                                  Container(
                                    margin: EdgeInsets.all(15.0),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Container(
                                          width: 100.0,
                                          height: 100.0,
                                          color: CustomColors.grey,
                                          child: Image(
                                            image: AssetImage(
                                                "assets/images/logo.png"),
                                          ),
                                        ),
                                        Flexible(
                                          child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Container(
                                              height: 100.0,
                                              margin:
                                                  EdgeInsets.only(left: 10.0),
                                              child: Text(
                                                "Greenhouse Wild Wonders Tomatoes, 12 Ounce",
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .bodyText2,
                                                textAlign: TextAlign.left,
                                                overflow: TextOverflow.visible,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        left: 15.0, right: 15.0, bottom: 15.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "\$3.00",
                                          style: TextStyle(
                                            fontSize: 20.0,
                                          ),
                                        ),
                                        Container(
                                          decoration: BoxDecoration(
                                            border: Border.all(width: 0.5),
                                            borderRadius:
                                                BorderRadius.circular(5.0),
                                            gradient: LinearGradient(
                                                begin: Alignment.topCenter,
                                                end: Alignment.bottomCenter,
                                                colors: [
                                                  CustomColors.lightYellow,
                                                  CustomColors.yellow
                                                ]),
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                25.0, 15.0, 25.0, 15.0),
                                            child: Text(
                                              "Add",
                                              style: TextStyle(
                                                fontSize: 16.0,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Container(
                                          height: 1.0,
                                          margin: EdgeInsets.only(
                                              left: 50.0,
                                              right: 50.0,
                                              bottom: 10.0),
                                          color: CustomColors.darkOffWhite,
                                        ),
                                        Container(
                                          margin: EdgeInsets.fromLTRB(
                                              15.0, 0.0, 15.0, 2.0),
                                          child: Text(
                                            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. "
                                            "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, "
                                            "when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
                                            style: TextStyle(
                                                color: CustomColors.grey,
                                                fontSize: 12.0,
                                                height: 1.3),
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.fromLTRB(
                                              15.0, 0.0, 15.0, 10.0),
                                          child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Text(
                                              "See product details...",
                                              style: TextStyle(
                                                color: CustomColors.blue,
                                                fontSize: 12.0,
                                              ),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.fromLTRB(
                                              15.0, 0.0, 0.0, 10.0),
                                          child: Row(
                                            mainAxisSize: MainAxisSize.max,
                                            children: [
                                              Text(
                                                "Customers also bought ",
                                                style: TextStyle(
                                                    color: CustomColors.grey,
                                                    fontSize: 14.0,
                                                    fontWeight:
                                                        FontWeight.w700),
                                              ),
                                              Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.5,
                                                height: 1.0,
                                                color: CustomColors.grey,
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(
                                              left: 15.0, right: 15.0),
                                          height: 100.0,
                                          child: ListView.builder(
                                              scrollDirection: Axis.horizontal,
                                              itemCount: 6,
                                              itemBuilder:
                                                  (BuildContext context,
                                                      int index) {
                                                return Container(
                                                  margin: EdgeInsets.only(
                                                      left: 2.0, right: 2.0),
                                                  width: 100.0,
                                                  height: 100.0,
                                                  color: CustomColors.grey,
                                                  child: Image(
                                                    image: AssetImage(
                                                        "assets/images/logo.png"),
                                                  ),
                                                );
                                              }),
                                        ),
                                        SizedBox(
                                          height: 15.0,
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    height: 1.0,
                                    color: CustomColors.darkOffWhite,
                                  ),
                                ],
                              ),
                            ),
                          );
                        }),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                ],
              ),
              Positioned(
                top: 120,
                right: 10,
                child: Visibility(
                  visible: showFilterExpansionTile,
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.8,
                    height: MediaQuery.of(context).size.height * 0.5,
                    color: Colors.grey.shade50,
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          GestureDetector(
                              onTap: () {
                                setState(() {
                                  showFilterExpansionTile = false;
                                });
                              },
                              child: Align(
                                  alignment: Alignment.topRight,
                                  child: Icon(Icons.close))),
                          ExpansionTile(
                            initiallyExpanded: true,
                            // backgroundColor: Colors.grey,
                            key: PageStorageKey('1'),
                            title: Text('Departments'),
                            children: [
                              CheckboxListTile(
                                title: Text('Grocery & Gourmet Food'),
                                selected: false,
                                value: false,
                                onChanged: (bool value) {},
                              ),
                              CheckboxListTile(
                                title: Text('Produce'),
                                value: false,
                                onChanged: (bool value) {},
                              ),
                            ],
                          ),
                          ExpansionTile(
                            key: PageStorageKey('2'),
                            title: Text('Sort'),
                            children: [
                              CheckboxListTile(
                                title: Text('Featured'),
                                selected: false,
                                value: false,
                                onChanged: (bool value) {},
                              ),
                              CheckboxListTile(
                                title: Text('Price: Low to High'),
                                value: false,
                                onChanged: (bool value) {},
                              ),
                              CheckboxListTile(
                                title: Text('Price: High to Low'),
                                value: false,
                                onChanged: (bool value) {},
                              ),
                              CheckboxListTile(
                                title: Text('Avg. Customer Review'),
                                value: false,
                                onChanged: (bool value) {},
                              ),
                            ],
                          ),
                          ExpansionTile(
                            key: PageStorageKey('3'),
                            title: Text('Store'),
                            children: [
                              CheckboxListTile(
                                title: Text('Amazon'),
                                selected: false,
                                value: false,
                                onChanged: (bool value) {},
                              ),
                              CheckboxListTile(
                                title: Text('Produce'),
                                value: false,
                                onChanged: (bool value) {},
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
