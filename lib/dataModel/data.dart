import 'package:flutter/cupertino.dart';

class Category {
  int id;
  String name = "";
  String slug = "";
  var photo = "";
  var storeId = "";
  var zipCode = "";

  //List<SubCategories> subCategories = [];

  Category(
      {@required this.id,
      @required this.name,
      this.slug,
      this.photo,
      this.storeId,
      this.zipCode});
}

class SubCategories {
  String sid = "";
  String name = "";

  // double price = 0;
  // String desc ="";
  List products = [];

  SubCategories({
    @required this.sid,
    @required this.name,
    @required this.products,
  });
}

class CategoriesClass {
  final List<Category> listOfCategories = [];

  List get categoriesList {
    return listOfCategories;
  }
}
