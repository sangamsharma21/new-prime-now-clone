//import 'package:flutter/cupertino.dart';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:prime_now_clone/ChangeLocation.dart';
import 'package:prime_now_clone/ProductList.dart';
import 'package:prime_now_clone/SearchScreen.dart';
import 'package:prime_now_clone/ShopByStore/ShopByDepartment.dart';
import 'package:prime_now_clone/TermsAndConditions.dart';
import 'package:prime_now_clone/Utils/colors.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';

import 'Utils/Urls.dart';

class DashboardScreen extends StatefulWidget {
  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
//  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  List<String> _wholeFoodMarket = [
    'Produce',
    'Meat & sea Food',
    'Dairy ,cheese & eggs',
    'Prepared Foods',
    'Beverages',
    'Snacks',
    'Pantry',
    'Frozen Foods',
  ];
  List<String> _foodBeverage = [
    'Fruits & Vegetables',
    'Dairy, cheese , & eggs',
    'Snacks and candy',
    'Prepared Foods',
    'Beverages',
    'Snacks',
    'Pantary',
    'Frozen Foods',
  ];

  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  var loading = false;
  List stores;
  List categoriesSubcategories = [];

  // List subcategories;

  Future<void> getStores() async {
    var response = await http.post(
      Urls.getStore,
      body: {
        'zipcode': '01801',
      },
    );

    try {
      setState(() {
        var convertJsonData = json.decode(response.body);
        if (convertJsonData['status'] == true) {
          loading = false;
          stores = convertJsonData['data'];
        } else {
          loading = false;
          Fluttertoast.showToast(
            msg: 'Something went wrong',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
          );
        }
      });
    } catch (e) {
      setState(() {
        loading = false;
        Fluttertoast.showToast(
          msg: e.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
        );
      });
    }
  }

  Future<void> getAllCategory() async {
    var response = await http.post(
      Urls.getAllCategorySubCategory,
      body: {
        'zipcode': '01801',
      },
    );

    try {
      setState(() {
        var convertJsonData = json.decode(response.body);
        if (convertJsonData['status'] == true) {
          loading = false;
          categoriesSubcategories = convertJsonData['data'];
          // for (int i = 0; i < categoriesSubcategories.length; i++) {
          //   subcategories =
          //       categoriesSubcategories[i]['category']['subcategories'];
          //   print('Subactegories are $subcategories');
          // }
        } else {
          loading = false;
          Fluttertoast.showToast(
            msg: 'Something went wrong',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
          );
        }
      });
    } catch (e) {
      setState(() {
        loading = false;
        Fluttertoast.showToast(
          msg: e.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
        );
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getStores();
    getAllCategory();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xfffefefe),
        drawer: CustomDrawer(),
        key: scaffoldKey,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(kToolbarHeight),
            child: CustomAppbar(
              scaffoldKey: this.scaffoldKey,
            )),
        body: SingleChildScrollView(
          child: Container(
            width: size.width,
            child: Stack(
              children: [
                Column(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ChangeLocation())),
                      child: Container(
                        margin: EdgeInsets.only(top: 10.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Shopping in',
                              style: TextStyle(
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.w300,
                                  color: Colors.black,
                                  fontFamily: 'Roboto'),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10.0),
                              child: Text(
                                '01801',
                                style: TextStyle(
                                    color: Color(0xffff7817),
                                    fontWeight: FontWeight.w500,
                                    fontSize: 14.0,
                                    fontFamily: 'Roboto'),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                left: 5.0,
                              ),
                              child: Icon(
                                Icons.location_on_rounded,
                                color: CustomColors.primeNowOrange,
                                size: 12.0,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SearchScreen()));
                      },
                      child: Container(
                        height: 50,
                        width: size.width,
                        decoration: BoxDecoration(
                            color: Colors.grey.shade100,
                            borderRadius: BorderRadius.circular(5.0)),
                        child: Row(
                          children: [
                            Container(
                              child: Icon(
                                Icons.search,
                                size: 18.0,
                                color: Colors.black54,
                              ),
                              margin: EdgeInsets.only(left: 10.0),
                            ),
                            Container(
                              child: Text(
                                'What are you looking for today ?',
                                style: TextStyle(
                                    fontSize: 14.0,
                                    fontWeight: FontWeight.w400,
                                    color: CustomColors.lightGrey,
                                    fontFamily: 'Roboto'),
                              ),
                              margin: EdgeInsets.only(left: 10.0),
                            ),
                            Spacer(),
                            Container(
                              margin: EdgeInsets.only(right: 10.0),
                              child: Icon(
                                Icons.mic,
                                color: Colors.black54,
                                size: 18.0,
                              ),
                            )
                          ],
                        ),
                        margin:
                            EdgeInsets.only(top: 15.0, left: 10, right: 10.0),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 15.0, right: 10, left: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'SHOP BY STORE',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 12.0,
                                fontFamily: 'Roboto'),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: size.height * 0.17,
                      width: size.width,
                      margin: EdgeInsets.only(top: 10.0, left: 10, right: 10),
                      child: stores != null
                          ? ListView.builder(
                              itemCount: stores.length,
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (context, int index) {
                                return GestureDetector(
                                  behavior: HitTestBehavior.opaque,
                                  onTap: () => Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              ShopByDepartment(
                                                storeId: stores[index]['id'],
                                              ))),
                                  child: Container(
                                    width: size.width / 3,
                                    margin: EdgeInsets.only(right: 5.0),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8.0),
                                    ),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(6.0),
                                      child: Image.network(
                                        Urls.DomainUrl +
                                            stores[index]['store_image'],
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                  ),
                                );
                              })
                          : Center(
                              child: CircularProgressIndicator(),
                            ),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 80),
                      child: categoriesSubcategories != null
                          ? ListView.builder(
                              itemCount: categoriesSubcategories.length,
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemBuilder: (context, int index) {
                                return Column(
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(
                                          top: 15.0, right: 10, left: 10),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            categoriesSubcategories[index]
                                                ['category']['name'],
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.w500,
                                                fontSize: 12.0,
                                                fontFamily: 'Roboto'),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      height: size.height * 0.22,
                                      width: size.width,
                                      margin: EdgeInsets.only(
                                        top: 10.0,
                                        left: 10,
                                        right: 10,
                                      ),
                                      child: ListView.builder(
                                          itemCount:
                                              categoriesSubcategories[index]
                                                          ['category']
                                                      ['subcategories']
                                                  .length,
                                          shrinkWrap: true,
                                          scrollDirection: Axis.horizontal,
                                          itemBuilder: (context, int y) {
                                            return GestureDetector(
                                              behavior: HitTestBehavior.opaque,
                                              onTap: () => Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          ProductList())),
                                              child: Container(
                                                width: size.width / 2.6,
                                                margin:
                                                    EdgeInsets.only(right: 6.0),
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          4.0),
                                                  image: DecorationImage(
                                                      image: NetworkImage(
                                                        'https://46ba123xc93a357lc11tqhds-wpengine.netdna-ssl.com/wp-content/uploads/2019/09/amazon-alexa-event-sept-2019.jpg',
                                                      ),
                                                      fit: BoxFit.fill),
                                                ),
                                                child: Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        top: 10, left: 10),
                                                    child: Text(
                                                      categoriesSubcategories[
                                                                      index]
                                                                  ['category']
                                                              ['subcategories']
                                                          [y]['name'],
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontFamily: 'Roboto',
                                                          fontSize: 14.0,
                                                          fontWeight:
                                                              FontWeight.w700),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            );
                                          }),
                                    ),
                                  ],
                                  mainAxisSize: MainAxisSize.min,
                                );
                              })
                          : Container(
                              height: size.height * 0.22,
                              child: Center(
                                child: CircularProgressIndicator(),
                              ),
                            ),
                    ),
                  ],
                ),
                Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: GestureDetector(
                      child: Container(
                        width: size.width,
                        height: 60,
                        color: Color(0xff353942),
                        child: Row(
                          children: [
                            Container(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text(
                                    'see all terms & conditions',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      fontFamily: 'Roboto',
                                      color: Colors.white,
                                    ),
                                  ),
                                  Text(
                                    'ON DEALS & SPECIAL OFFERS',
                                    style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 10,
                                      fontFamily: 'Roboto',
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                              margin: EdgeInsets.only(left: 10.0),
                            ),
                            Spacer(),
                            Container(
                              margin: EdgeInsets.only(right: 15.0),
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.white,
                                size: 20.0,
                              ),
                            )
                          ],
                        ),
                      ),
                      behavior: HitTestBehavior.opaque,
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => TermsAndCondition()));
                      },
                    ))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
