import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';

class ReturnPolicy extends StatelessWidget{

  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context){
    return Scaffold(
      key: scaffoldKey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: CustomAppbar(scaffoldKey: this.scaffoldKey,),
      ),
      drawer: CustomDrawer(),
      backgroundColor: Colors.grey.shade300,
      body: Container(
        padding: EdgeInsets.all(15.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: Text('Return Policy', style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 24.0,
                  color: Colors.black,
                ),
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Text(
                'Amazon Prime Now offers returns for items within 30 days of receipt of shipment. Most items sold through Amazon Prime Now follow Amazon\'s general returns policies, but some products have different policies or requirements associated with them.\n'
                ,style: TextStyle(
                fontSize: 15.0,
              ),
                // textAlign: TextAlign.
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  'Gift Cards'
                  ,style: TextStyle(
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                ),
                  // textAlign: TextAlign.
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  'Gift Cards are not returnable after purchase (except as required by law).\n'
                  ,style: TextStyle(
                  fontSize: 15.0,
                  // fontWeight: FontWeight.bold,
                ),
                  // textAlign: TextAlign.
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  'Grocery'
                  ,style: TextStyle(
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                ),
                  // textAlign: TextAlign.
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  'Grocery items are not returnable but may be refunded.\n'
                  ,style: TextStyle(
                  fontSize: 15.0,
                  // fontWeight: FontWeight.bold,
                ),
                  // textAlign: TextAlign.
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  'Hazardous Materials'
                  ,style: TextStyle(
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                ),
                  // textAlign: TextAlign.
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  'Hazardous materials, including flammable liquids or gases, are not returnable but may be refunded. Contact the manufacturer directly for service or warranty information.\n'
                  ,style: TextStyle(
                  fontSize: 15.0,
                  // fontWeight: FontWeight.bold,
                ),
                  // textAlign: TextAlign.
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  'Computers (laptops & desktops)'
                  ,style: TextStyle(
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                ),
                  // textAlign: TextAlign.
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  '1. New desktops, laptops or tablets (excluding Kindle E-readers and Fires) purchased from Amazon Prime Now that didn\'t start when they arrived, arrived in damaged condition, or are still in an unopened box can be returned for a full refund within 30 days of purchase.\n'
                  ,style: TextStyle(
                  fontSize: 15.0,
                  // fontWeight: FontWeight.bold,
                ),
                  // textAlign: TextAlign.
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  '2. Amazon Prime Now may test computers that are returned because they didn\'t start when they arrived and impose a customer fee equal to 15 percent of the product sales price if the customer misrepresents the condition of the product.\n'
                  ,style: TextStyle(
                  fontSize: 15.0,
                  // fontWeight: FontWeight.bold,
                ),
                  // textAlign: TextAlign.
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  '3. Any returned desktop, laptop or tablet that is damaged through customer misuse, is missing parts, or is in unsellable condition due to customer tampering may result in the customer being charged a higher restocking fee based on the condition of the product.\n'
                  ,style: TextStyle(
                  fontSize: 15.0,
                  // fontWeight: FontWeight.bold,
                ),
                  // textAlign: TextAlign.
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  'Flowers'
                  ,style: TextStyle(
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                ),
                  // textAlign: TextAlign.
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  'Fresh flowers are not returnable to Amazon Prime Now, but may be refundable.\n'
                  ,style: TextStyle(
                  fontSize: 15.0,
                  // fontWeight: FontWeight.bold,
                ),
                  // textAlign: TextAlign.
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  'Alcohol Products'
                  ,style: TextStyle(
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                ),
                  // textAlign: TextAlign.
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  'Alcohol products are not returnable to Amazon Prime Now, but may be refundable.\n'
                  ,style: TextStyle(
                  fontSize: 15.0,
                  // fontWeight: FontWeight.bold,
                ),
                  // textAlign: TextAlign.
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  'Items Sold by Sellers'
                  ,style: TextStyle(
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                ),
                  // textAlign: TextAlign.
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  'Sellers may determine that certain products offered through their stores are not returnable but may be refundable.\n'
                  ,style: TextStyle(
                  fontSize: 15.0,
                  // fontWeight: FontWeight.bold,
                ),
                  // textAlign: TextAlign.
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  'Amazon Restaurants Items'
                  ,style: TextStyle(
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                ),
                  // textAlign: TextAlign.
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  'Amazon Restaurants deliveries are not returnable but may be refunded.\n'
                  ,style: TextStyle(
                  fontSize: 15.0,
                  // fontWeight: FontWeight.bold,
                ),
                  // textAlign: TextAlign.
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}