import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';

class CheckReturnStatus extends StatelessWidget{

  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context){
    return Scaffold(
      key: scaffoldKey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: CustomAppbar(scaffoldKey: this.scaffoldKey,),
      ),
      drawer: CustomDrawer(),
      backgroundColor: Colors.grey.shade300,
      body: Container(
        padding: EdgeInsets.all(15.0),
        child: Column(
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Text('Check Your Return Status', style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 24.0,
                color: Colors.black,
              ),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              'To check the status of your return, please contact Amazon Prime Now Customer Service.\n\n'
                  'To contact Customer Service, go to Contact Us. In the app you can access this page from the main menu. On Primenow.com you can get to this page by going to Help from the Your Account Menu.\n', style: TextStyle(
              fontSize: 15.0,
            ),
              // textAlign: TextAlign.
            ),
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                      text: "Note: ", style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  )
                  ),
                  TextSpan(
                      text: 'After the carrier has received your item, it can take up to 2 weeks to receive and process your return.', style: TextStyle(
                      color: Colors.black
                  )
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}