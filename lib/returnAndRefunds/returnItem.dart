import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';

class ReturnItem extends StatelessWidget{

  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context){
    return Scaffold(
      key: scaffoldKey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: CustomAppbar(scaffoldKey: this.scaffoldKey,),
      ),
      drawer: CustomDrawer(),
      backgroundColor: Colors.grey.shade300,
      body: Container(
        padding: EdgeInsets.all(15.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: Text('Return an Item', style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 24.0,
                  color: Colors.black,
                ),
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Text(
                'You can arrange a return for most items through the Amazon Prime Now app or website. If the return can\'t be arranged through those places (for example, in the case of Amazon Restaurants), please contact Amazon Prime Now Customer Service.\n\n'
                    'To arrange a return via the app or website:\n\n'
                    '1. Go to Your Orders.\n'
                    'In the app Your Orders can be accessed through the main menu.\n'
                    'On Primenow.com Your Orders can be found in the Your Account menu\n'
                    '2. Select the order containing the item.\n'
                    '3. You\'ll see a Return option underneath the item if a return can be initiated through the app or website for the item.'
                '4. If the option isn\'t available, go to Contact Us to reach out to Amazon Prime Now Customer Service.\n'
                'In the app you can access this page from the main menu.\n'
                'On Primenow.com you can get to this page by going to Help from the Your Account Menu.\n', style: TextStyle(
                fontSize: 15.0,
              ),
                // textAlign: TextAlign.
              ),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: "Note: ", style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    )
                    ),
                    TextSpan(
                      text: 'Please see our Return Policy for return time frames and information about what items can be returned. After the carrier has received your item, it can take up to 2 weeks to receive and process your return.', style: TextStyle(
                      color: Colors.black
                    )
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}