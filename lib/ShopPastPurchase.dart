import 'package:flutter/material.dart';
import 'package:prime_now_clone/Utils/colors.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';

class ShopPastPurchase extends StatefulWidget {
  @override
  _ShopPastPurchaseState createState() => _ShopPastPurchaseState();
}

class _ShopPastPurchaseState extends State<ShopPastPurchase> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      drawer: CustomDrawer(),
      key: scaffoldKey,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(kToolbarHeight),
          child: CustomAppbar(
            scaffoldKey: this.scaffoldKey,
          )),
      body: Container(
        height: size.height,
        width: size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              child: Text(
                'Your Past Purchase',
                style: TextStyle(
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.w400,
                    color: Colors.black54,
                    fontSize: 16.0),
              ),
              margin: EdgeInsets.all(10.0),
            ),
            Container(
              child: Text(
                'You have no purchase to show',
                style: TextStyle(
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 14.0),
              ),
              margin: EdgeInsets.all(10.0),
            ),
            Container(
              child: Text(
                'No items are shown because you have not purchased any items in Amazon Prime Now. your purchased '
                'items don not  meet the filter criteria, or they are not currently available for re-purchase in'
                ' the ZIP code .',
                style: TextStyle(
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.normal,
                    color: Colors.black,
                    fontSize: 12.0),
              ),
              margin: EdgeInsets.all(10.0),
            ),
            Container(
              width: size.width,
              margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              height: 40,
              decoration: BoxDecoration(
                border: Border.all(
                  width: 0.5,
                ),
                borderRadius: BorderRadius.circular(5.0),
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [CustomColors.lightYellow, CustomColors.yellow]),
              ),
              child: Center(
                child: Text(
                  "Continue Shopping",
                  style: TextStyle(
                      fontSize: 16.0,
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.w300),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
