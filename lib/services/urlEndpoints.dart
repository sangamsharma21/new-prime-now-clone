var product_id = 237;

class EndPoints {
  String get baseUrl {
    return "http://oceansaks.com/";
  }

  String get userRegister => baseUrl + "api/user/register";

  String get verifyOtp => baseUrl + "api/user/verify-otp";

  String get userLogin => baseUrl + "api/user/login";

  String get allCategoriesByZipCode => baseUrl + "api/fetch-all-categories";

  String get fetchAllLocations => baseUrl + "api/get-locations";

  String get fetchAllCategoriesByZipCodeAndStore =>
      baseUrl + "api/fetch-all-categories-by-zipcode-store";

  String get storeByZipCode => baseUrl + "api/fetch-store";

  String get fetchAllSubCategories => baseUrl + "api/fetch-all-subcategories";

  String get fetchALlCategoriesAndSubCategories =>
      baseUrl + "api/fetch-all-categories-and-subcategories";

  String get fetchAllCategoriesAndSubcategoriesByStore =>
      baseUrl + "api/fetch-all-categories-and-subcategories-by-store";

  String get fetchProductsByCategoryAndStoreId =>
      baseUrl + "api/fetch-products-by-category-by-store";

  String get fetchProductsBySubcategoryAndStoreId =>
      baseUrl + "api/fetch-products-by-subcategory-by-store";

  String get productDetails =>
      baseUrl + "api/api/fetch-product-detail/$product_id";

  String get bestSellerProductByStore =>
      baseUrl + "api/fetch-best-seller-products";
}
