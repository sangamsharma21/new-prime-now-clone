import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

import 'package:prime_now_clone/services/urlEndpoints.dart';
import 'package:prime_now_clone/dataModel/data.dart';

class ApiHandler {
  EndPoints _endPoints = EndPoints();

  Future<bool> registerUser() async {
    try {
      var response = await http.get(_endPoints.userRegister);
      if (response.statusCode == HttpStatus.ok)
        return true;
      else
        throw response.statusCode;
    } catch (error) {
      print(error);
      throw error;
    }
  }

  Future<bool> verifyOtp() async {
    try {
      var response = await http.post(_endPoints.verifyOtp);
      if (response.statusCode == HttpStatus.ok)
        return true;
      else
        throw response.statusCode;
    } catch (error) {
      throw error;
    }
  }

  Future logIn() async {
    try {
      var response = await http.post(_endPoints.userLogin);
    } catch (error) {
      throw error;
    }
  }

  Future fetchAllLocations() async {
    try {
      var response = await http.get(_endPoints.fetchAllLocations);
      if (response.statusCode == HttpStatus.ok) {}
    } catch (error) {
      throw error;
    }
  }

  Future<void> fetchAllCategoriesByZipCode() async {
    CategoriesClass categoriesInstance = CategoriesClass();
    try {
      var response = await http
          .post(_endPoints.allCategoriesByZipCode, body: {"zipcode": "01801"});
      //print(response.body);
      if (response.statusCode == HttpStatus.ok) {
        Map<String, dynamic> responseBody = json.decode(response.body);

        List<Map<String, dynamic>> data = responseBody["data"];
        print(data);
        data.map((e) => categoriesInstance.categoriesList.add(Category(
              id: e["id"],
              name: e["name"],
              slug: e["slug"],
              photo: e["photo"],
              storeId: e["store_id"],
              zipCode: e["zipcode"],
            )));
      }
    } catch (error) {
      print(error);
    }
    print(categoriesInstance.categoriesList);
  }
}
