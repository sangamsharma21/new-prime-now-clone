import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:prime_now_clone/Home.dart';
import 'package:prime_now_clone/Utils/colors.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.white,
        statusBarIconBrightness: Brightness.dark));
    return MaterialApp(
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: CustomColors.darkGrey,
        primaryColorLight: CustomColors.lightGrey,
        accentColor: CustomColors.orange,
        cardColor: CustomColors.offWhite,
        backgroundColor: CustomColors.white,
        canvasColor: CustomColors.white,
        fontFamily: 'Roboto',
        textTheme: TextTheme(
          headline1: TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.bold,
              color: CustomColors.darkGrey),
          headline2: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
              color: CustomColors.white),
          headline3: TextStyle(
              fontSize: 14.0,
              fontWeight: FontWeight.bold,
              color: CustomColors.white),
          headline4: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
              color: CustomColors.darkGrey),
          headline5: TextStyle(
              fontSize: 14.0,
              fontWeight: FontWeight.bold,
              color: CustomColors.orange),
          headline6: TextStyle(
              fontSize: 14.0,
              fontWeight: FontWeight.bold,
              color: CustomColors.darkGrey),
          bodyText2: TextStyle(fontSize: 14.0, color: CustomColors.lightGrey),
        ),
      ),
      debugShowCheckedModeBanner: false,
      home: Home(),
    );
  }
}
