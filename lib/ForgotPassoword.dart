import 'package:flutter/material.dart';
import 'package:prime_now_clone/Utils/colors.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        automaticallyImplyLeading: false,
        backgroundColor: Color(0xffecedf1),
        elevation: 1.0,
        title: Image.asset(
          'assets/images/oceansaks.png',
          width: 80,
          height: 40,
        ),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              child: Text(
                'Password Assistance',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Roboto',
                    color: Colors.black,
                    fontSize: 18.0),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10.0),
              child: Text(
                'Enter the email address or mobile phone number associated with your  account',
                style: TextStyle(
                    fontSize: 14.0,
                    color: Colors.black54,
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.normal),
              ),
            ),
            Container(
              padding: EdgeInsets.all(0.0),
              width: size.width,
              margin: EdgeInsets.only(
                top: 10,
              ),
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Colors.grey.shade300)),
              child: TextFormField(
                decoration: InputDecoration(
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    hintText: 'Your email or mobile number',
                    contentPadding: EdgeInsets.fromLTRB(15, 0, 10, 0),
                    hintStyle: TextStyle(
                        color: Colors.grey.shade400,
                        fontFamily: 'Roboto',
                        fontSize: 16.0)),
              ),
            ),
            GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {},
              child: Container(
                width: size.width,
                height: 43,
                margin: EdgeInsets.only(top: 20),
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 0.5,
                  ),
                  borderRadius: BorderRadius.circular(5.0),
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [CustomColors.lightYellow, CustomColors.yellow]),
                ),
                child: Center(
                  child: Text(
                    "Continue",
                    style: TextStyle(
                        fontSize: 15.0,
                        fontFamily: 'Roboto',
                        fontWeight: FontWeight.w400),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
