import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:prime_now_clone/AllDepartmentsList.dart';
import 'package:prime_now_clone/ChangeLocation.dart';
import 'package:prime_now_clone/Utils/colors.dart';

import 'Utils/Urls.dart';

class Cart extends StatefulWidget {
  @override
  _CartState createState() => _CartState();
}

class _CartState extends State<Cart> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  List products;

  Future<void> getCartItems() async {
    var response = await http.post(
      Urls.getCart,
      body: {
        'user_id': '9',
      },
    );

    try {
      setState(() {
        var convertJsonData = json.decode(response.body);
        if (convertJsonData['status'] == true) {
          products = convertJsonData['data'];
        } else {
          Fluttertoast.showToast(
            msg: 'Something went wrong',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
          );
        }
      });
    } catch (e) {
      setState(() {
        Fluttertoast.showToast(
          msg: e.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
        );
      });
    }
  }

  Future<void> updateCart(otp) async {
    var response = await http.post(
      Urls.updateCart,
      body: {'user_id': '9', 'product_id': '237', 'cart_id': '14', 'qty': '2'},
    );

    try {
      setState(() {
        var convertJsonData = json.decode(response.body);
        if (convertJsonData['status'] == true) {
          Fluttertoast.showToast(
            msg: 'Cart Updated Successfully',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
          );
        } else {
          Fluttertoast.showToast(
            msg: 'Something went wrong',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
          );
        }
      });
    } catch (e) {
      setState(() {
        Fluttertoast.showToast(
          msg: e.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
        );
      });
    }
  }

  Future<void> deleteCart() async {
    var response = await http.post(
      Urls.deleteCart,
      body: {
        'cart_id': '15',
        'user_id': '9',
      },
    );

    try {
      setState(() {
        var convertJsonData = json.decode(response.body);
        if (convertJsonData['status'] == true) {
          Fluttertoast.showToast(
            msg: 'Removed  Successfully',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
          );
        } else {
          Fluttertoast.showToast(
            msg: 'Something went wrong',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
          );
        }
      });
    } catch (e) {
      setState(() {
        Fluttertoast.showToast(
          msg: e.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
        );
      });
    }
  }

  String categoryType = 'Rent';
  String categoryid = '1';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getCartItems();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Theme.of(context).backgroundColor,
        elevation: 0,
        leading: IconButton(
          onPressed: () => _scaffoldKey.currentState.openDrawer(),
          icon: Icon(
            Icons.menu,
            color: Theme.of(context).primaryColor,
          ),
        ),
        title: Container(
          width: MediaQuery.of(context).size.width,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 0.6,
                child: Center(
                  child: Container(
                      height: 35.0,
                      child:
                          Image(image: AssetImage("assets/images/logo.png"))),
                ),
              ),
              Icon(
                Icons.shopping_cart_outlined,
                color: Theme.of(context).primaryColor,
              )
            ],
          ),
        ),
      ),
      drawer: Drawer(
        child: Container(
          color: CustomColors.darkGrey,
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              DrawerHeader(
                  padding: EdgeInsets.zero,
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          height: 50.0,
                          child: Image(
                            image: AssetImage("assets/images/logo.png"),
                          ),
                        ),
                        Center(
                          child: Container(
                            margin: EdgeInsets.only(top: 20.0),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  "Shopping in ",
                                  style: TextStyle(
                                      color: CustomColors.orange,
                                      fontWeight: FontWeight.w700,
                                      fontSize: 16),
                                ),
                                Text(
                                  "45014 ",
                                  style: TextStyle(
                                      color: CustomColors.orange,
                                      fontWeight: FontWeight.w700,
                                      fontSize: 16),
                                ),
                                Icon(
                                  Icons.location_pin,
                                  color: CustomColors.lightGrey,
                                  size: 16,
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  )),
              GestureDetector(
                onTap: () {},
                child: ListTile(
                  title: Text(
                    "Home",
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.3,
                child: Divider(
                  height: 1.0,
                  color: CustomColors.white,
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: ListTile(
                  title: Text(
                    "Shop Past Purchases",
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.3,
                child: Divider(
                  height: 1.0,
                  color: CustomColors.white,
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AllDepartmentList()));
                },
                child: ListTile(
                  title: Text(
                    "Shop by Department",
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.3,
                child: Divider(
                  height: 1.0,
                  color: CustomColors.white,
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: ListTile(
                  title: Text(
                    "Your Orders",
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.3,
                child: Divider(
                  height: 1.0,
                  color: CustomColors.white,
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: ListTile(
                  title: Text(
                    "Your Account",
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.3,
                child: Divider(
                  height: 1.0,
                  color: CustomColors.white,
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: ListTile(
                  title: Text(
                    "Help & About",
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.3,
                child: Divider(
                  height: 1.0,
                  color: CustomColors.white,
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: ListTile(
                  title: Text(
                    "Contact Us",
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.3,
                child: Divider(
                  height: 1.0,
                  color: CustomColors.white,
                ),
              ),
              GestureDetector(
                onTap: () => Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ChangeLocation())),
                child: ListTile(
                  title: Text(
                    "Change Location",
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.3,
                child: Divider(
                  height: 1.0,
                  color: CustomColors.white,
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: ListTile(
                  title: Text(
                    "Sign Out",
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
              ),
              Opacity(
                opacity: 0.3,
                child: Divider(
                  height: 1.0,
                  color: CustomColors.white,
                ),
              ),
              ListTile()
            ],
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: products != null
            ? Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: ListView.builder(
                    itemCount: products.length,
                    itemBuilder: (context, int index) {
                      return Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          ListTile(
                            leading: Image.network(
                              Urls.DomainUrl + products[index]['item']['photo'],
                              fit: BoxFit.fill,
                              width: 120,
                              height: 120,
                            ),
                            title: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 10.0, right: 10.0),
                                  child: Text(
                                    products[index]['item']['name'],
                                    style: TextStyle(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.normal,
                                        fontSize: 14.0),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.all(5.0),
                                  child: Text(
                                    '\$ ${products[index]['item']['price'].toString()}',
                                    style: TextStyle(
                                        color: Colors.red,
                                        fontWeight: FontWeight.normal,
                                        fontSize: 12.0),
                                  ),
                                ),
                                Row(
                                  children: [
                                    Container(
                                      height: 35,
                                      margin: EdgeInsets.only(left: 5.0),
                                      decoration: BoxDecoration(
                                        border: Border.all(color: Colors.grey),
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                      ),
                                      child: Row(
                                        children: [
                                          Container(
                                            decoration: BoxDecoration(
                                                color: Colors.grey.shade300,
                                                borderRadius: BorderRadius.only(
                                                    bottomRight:
                                                        Radius.circular(
                                                      2.0,
                                                    ),
                                                    topLeft: Radius.circular(
                                                      5.0,
                                                    ),
                                                    bottomLeft: Radius.circular(
                                                      5.0,
                                                    ),
                                                    topRight: Radius.circular(
                                                      2.0,
                                                    ))),
                                            child: Padding(
                                              padding: EdgeInsets.all(10.0),
                                              child: Icon(
                                                Icons.delete,
                                                color: Colors.black,
                                                size: 15.0,
                                              ),
                                            ),
                                          ),
                                          Container(
                                            child: Padding(
                                              padding: EdgeInsets.symmetric(
                                                  vertical: 10, horizontal: 15),
                                              child: Text(
                                                products[index]['size_qty'],
                                                style: TextStyle(
                                                    color: Colors.blue),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            decoration: BoxDecoration(
                                                color: Colors.grey.shade300,
                                                borderRadius: BorderRadius.only(
                                                    bottomRight:
                                                        Radius.circular(
                                                      5.0,
                                                    ),
                                                    topLeft: Radius.circular(
                                                      2.0,
                                                    ),
                                                    bottomLeft: Radius.circular(
                                                      2.0,
                                                    ),
                                                    topRight: Radius.circular(
                                                      5.0,
                                                    ))),
                                            child: Padding(
                                              padding: EdgeInsets.all(10.0),
                                              child: Icon(
                                                Icons.add,
                                                color: Colors.black,
                                                size: 15.0,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    GestureDetector(
                                      behavior: HitTestBehavior.opaque,
                                      onTap: () {
                                        deleteCart().whenComplete(() {
                                          setState(() {
                                            products.removeAt(index);
                                          });
                                        });
                                      },
                                      child: Container(
                                        height: 35,
                                        margin: EdgeInsets.only(left: 15.0),
                                        decoration: BoxDecoration(
                                            border:
                                                Border.all(color: Colors.grey),
                                            borderRadius:
                                                BorderRadius.circular(5.0),
                                            color: Colors.grey.shade100),
                                        child: Center(
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Text(
                                              'Remove',
                                              style: TextStyle(
                                                fontWeight: FontWeight.normal,
                                                color: Colors.black,
                                                fontSize: 14.0,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                          Divider()
                        ],
                      );
                    }),
              )
            : Container(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      child: Text(
                        'Your Shopping Cart is empty.',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 20.0,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10.0),
                      child: Text(
                        ' Your Shopping Cart lives to serve . Give it purpose -fill it with daily essentials ,gifts and more',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.black54,
                            fontSize: 15.0,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.normal),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                      height: 40,
                      decoration: BoxDecoration(
                        border: Border.all(
                          width: 0.5,
                        ),
                        borderRadius: BorderRadius.circular(5.0),
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              CustomColors.lightYellow,
                              CustomColors.yellow
                            ]),
                      ),
                      child: Center(
                        child: Text(
                          "Continue Shopping",
                          style: TextStyle(
                              fontSize: 16.0,
                              fontFamily: 'Roboto',
                              fontWeight: FontWeight.normal),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}
