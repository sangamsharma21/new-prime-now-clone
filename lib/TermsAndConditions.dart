import 'package:flutter/material.dart';
import 'package:prime_now_clone/Utils/colors.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';

import 'Utils/customAppbar.dart';

class TermsAndCondition extends StatefulWidget {
  @override
  _TermsAndConditionState createState() => _TermsAndConditionState();
}

class _TermsAndConditionState extends State<TermsAndCondition> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: CustomDrawer(),
        key: scaffoldKey,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(kToolbarHeight),
            child: CustomAppbar(
              scaffoldKey: this.scaffoldKey,
            )),
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  margin: EdgeInsets.all(15.0),
                  child: Text(
                    "Terms and Conditions",
                    style: Theme.of(context).textTheme.headline1,
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 1.0,
                  color: CustomColors.darkOffWhite,
                ),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 1.0,
                  margin: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 10.0),
                  color: CustomColors.darkOffWhite,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 10.0),
                  child: Text(
                    "Terms and Conditions",
                    style: Theme.of(context).textTheme.headline4,
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 10.0),
                  child: Text(
                    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. "
                    "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, "
                    "when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
                    style: Theme.of(context).textTheme.bodyText2,
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 1.0,
                  margin: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 10.0),
                  color: CustomColors.darkOffWhite,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 10.0),
                  child: Text(
                    "Terms and Conditions",
                    style: Theme.of(context).textTheme.headline4,
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 10.0),
                  child: Text(
                    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. "
                    "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, "
                    "when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
                    style: Theme.of(context).textTheme.bodyText2,
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 1.0,
                  margin: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 10.0),
                  color: CustomColors.darkOffWhite,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 10.0),
                  child: Text(
                    "Terms and Conditions",
                    style: Theme.of(context).textTheme.headline4,
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 10.0),
                  child: Text(
                    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. "
                    "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, "
                    "when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
                    style: Theme.of(context).textTheme.bodyText2,
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 1.0,
                  margin: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 10.0),
                  color: CustomColors.darkOffWhite,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 10.0),
                  child: Text(
                    "Terms and Conditions",
                    style: Theme.of(context).textTheme.headline4,
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 10.0),
                  child: Text(
                    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. "
                    "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, "
                    "when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
                    style: Theme.of(context).textTheme.bodyText2,
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 1.0,
                  margin: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 10.0),
                  color: CustomColors.darkOffWhite,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 10.0),
                  child: Text(
                    "Terms and Conditions",
                    style: Theme.of(context).textTheme.headline4,
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 10.0),
                  child: Text(
                    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. "
                    "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, "
                    "when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
                    style: Theme.of(context).textTheme.bodyText2,
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
