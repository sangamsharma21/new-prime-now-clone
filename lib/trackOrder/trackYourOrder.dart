import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';

class TrackYourOrder extends StatelessWidget{

  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context){
    return Scaffold(
      key: scaffoldKey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: CustomAppbar(scaffoldKey: this.scaffoldKey,),
      ),
      drawer: CustomDrawer(),
      backgroundColor: Colors.grey.shade300,
      body: Container(
        padding: EdgeInsets.all(15.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: Text('Track Your Order', style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 24.0,
                  color: Colors.black,
                ),
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Text(
                'You can track your Amazon Prime Now or Amazon Restaurants order in Your Orders.\n\n'
                    'To track your order:\n\n'
                    '1. Go to Your Orders.\n'
                    'In the app Your Orders can be accessed through the main menu.\n'
                    'On Primenow.com Your Orders can be found in the Your Account menu\n'
                    '2. Select the order you want to track. In the app you’ll need to tap Track the status of this order to see the tracking information.\n', style: TextStyle(
                fontSize: 15.0,
              ),
                // textAlign: TextAlign.
              ),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                        text: "Note: ", style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    )
                    ),
                    TextSpan(
                        text: 'You\'ll see a map showing both the delivery address and where your order is currently. You\'ll be able to watch your order as it moves towards you and be able to contact your courier after pick-up if you have questions about where your delivery is.', style: TextStyle(
                        color: Colors.black
                    )
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}