import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:prime_now_clone/SearchScreen.dart';
import 'package:prime_now_clone/Utils/Urls.dart';
import 'package:prime_now_clone/Utils/colors.dart';
import 'package:prime_now_clone/Utils/customAppbar.dart';
import 'package:prime_now_clone/Utils/customDrawer.dart';

class ShopByDepartment extends StatefulWidget {
  var storeId;

  ShopByDepartment({this.storeId});

  @override
  _ShopByDepartmentState createState() => _ShopByDepartmentState();
}

class _ShopByDepartmentState extends State<ShopByDepartment> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  List shopNames = [
    'Produce',
    'Meat,Sea Food & Dell',
    'Prepared Foods',
    'Dairy & Eggs',
    'Bakery',
    'Pantry',
    'Snacks & Sweets',
    'Frozen Foods'
  ];

  List categoriesSubcategories = [];
  List bestSellarProducts = [];

  Future<void> getAllCategory() async {
    var response = await http.post(
      Urls.getCategoryByStore,
      body: {'zipcode': '01801', 'store_id': '${widget.storeId}'},
    );

    try {
      setState(() {
        var convertJsonData = json.decode(response.body);
        if (convertJsonData['status'] == true) {
          categoriesSubcategories = convertJsonData['data'];
        } else {
          Fluttertoast.showToast(
            msg: 'Something went wrong',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
          );
        }
      });
    } catch (e) {
      setState(() {
        Fluttertoast.showToast(
          msg: e.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
        );
      });
    }
  }

  Future<void> getBestSellarProduct() async {
    var response = await http.post(
      Urls.bestSellarProducts,
      body: {'zipcode': '01801', 'store_id': '${widget.storeId}'},
    );

    try {
      setState(() {
        var convertJsonData = json.decode(response.body);
        if (response.statusCode == 200) {
          bestSellarProducts = convertJsonData;
          print('Best sellar Products $bestSellarProducts');
        } else {
          Fluttertoast.showToast(
            msg: 'Something went wrong',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
          );
        }
      });
    } catch (e) {
      setState(() {
        Fluttertoast.showToast(
          msg: e.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
        );
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getAllCategory();
    getBestSellarProduct();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      drawer: CustomDrawer(),
      key: scaffoldKey,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(kToolbarHeight),
          child: CustomAppbar(
            scaffoldKey: this.scaffoldKey,
          )),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => SearchScreen()));
                },
                child: Container(
                  height: size.height / 5,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(
                            'images/backimage.jpg',
                          ),
                          fit: BoxFit.fill)),
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      height: 50,
                      width: size.width,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5.0)),
                      child: Row(
                        children: [
                          Container(
                            child: Icon(
                              Icons.search,
                              size: 18.0,
                              color: Colors.black54,
                            ),
                            margin: EdgeInsets.only(left: 10.0),
                          ),
                          Container(
                            child: Text(
                              'Search Whole Foods Market',
                              style: TextStyle(
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.w400,
                                  color: CustomColors.lightGrey,
                                  fontFamily: 'Roboto'),
                            ),
                            margin: EdgeInsets.only(left: 10.0),
                          ),
                          Spacer(),
                          Container(
                            margin: EdgeInsets.only(right: 10.0),
                            child: Icon(
                              Icons.mic,
                              color: Colors.black54,
                              size: 18.0,
                            ),
                          )
                        ],
                      ),
                      margin:
                          EdgeInsets.only(left: 10, right: 10.0, bottom: 10),
                    ),
                  ),
                ),
              ),
              Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 15.0, right: 10, left: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'BESTSELLARS IN STORE',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                              fontSize: 12.0,
                              fontFamily: 'Roboto'),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: size.height * 0.22,
                    width: size.width,
                    margin: EdgeInsets.only(
                      top: 10.0,
                      left: 10,
                      right: 10,
                    ),
                    child: bestSellarProducts != null
                        ? ListView.builder(
                            itemCount: bestSellarProducts.length,
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (context, int y) {
                              return GestureDetector(
                                behavior: HitTestBehavior.opaque,
                                child: Container(
                                  width: size.width / 2.6,
                                  margin: EdgeInsets.only(right: 6.0),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(4.0),
                                    image: DecorationImage(
                                        image: NetworkImage(
                                          Urls.DomainUrl +
                                              bestSellarProducts[y]
                                                  ['thumbnail'],
                                        ),
                                        fit: BoxFit.fill),
                                  ),
                                  child: Align(
                                    alignment: Alignment.topLeft,
                                    child: Container(
                                      margin:
                                          EdgeInsets.only(top: 10, left: 10),
                                      child: Text(
                                        bestSellarProducts[y]['name'],
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'Roboto',
                                            fontSize: 14.0,
                                            fontWeight: FontWeight.w700),
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            })
                        : Center(
                            child: CircularProgressIndicator(),
                          ),
                  ),
                ],
                mainAxisSize: MainAxisSize.min,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'SHOP BY AISLE',
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontFamily: 'Roboto',
                        color: Colors.black,
                        fontSize: 12.0,
                      ),
                    )
                  ],
                ),
              ),
              ListView.builder(
                  itemCount: categoriesSubcategories.length,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (context, int index) {
                    return Column(
                      children: [
                        Container(
                          margin:
                              EdgeInsets.only(top: 15.0, right: 10, left: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                categoriesSubcategories[index]['category']
                                    ['name'],
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 12.0,
                                    fontFamily: 'Roboto'),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: size.height * 0.22,
                          width: size.width,
                          margin: EdgeInsets.only(
                            top: 10.0,
                            left: 10,
                            right: 10,
                          ),
                          child: ListView.builder(
                              itemCount: categoriesSubcategories[index]
                                      ['category']['subcategories']
                                  .length,
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (context, int y) {
                                return GestureDetector(
                                  behavior: HitTestBehavior.opaque,
                                  // onTap: () => Navigator.push(
                                  //     context,
                                  //     MaterialPageRoute(
                                  //         builder: (context) =>
                                  //             ProductList())),
                                  child: Container(
                                    width: size.width / 2.6,
                                    margin: EdgeInsets.only(right: 6.0),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(4.0),
                                      image: DecorationImage(
                                          image: NetworkImage(
                                            'https://46ba123xc93a357lc11tqhds-wpengine.netdna-ssl.com/wp-content/uploads/2019/09/amazon-alexa-event-sept-2019.jpg',
                                          ),
                                          fit: BoxFit.fill),
                                    ),
                                    child: Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        margin:
                                            EdgeInsets.only(top: 10, left: 10),
                                        child: Text(
                                          categoriesSubcategories[index]
                                                  ['category']['subcategories']
                                              [y]['name'],
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontFamily: 'Roboto',
                                              fontSize: 14.0,
                                              fontWeight: FontWeight.w700),
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              }),
                        ),
                      ],
                      mainAxisSize: MainAxisSize.min,
                    );
                  })
            ],
          ),
        ),
      ),
    );
  }
}
